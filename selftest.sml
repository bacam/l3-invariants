open HolKernel Parse boolLib bossLib
open testutils
open CoreTactics

(* For interactive use
fun die s = print (s^"\n")
*)

fun dummy_final _ _ _ _ = failwith "Ended up in dummy_final";

val empty_proofs : (term,thm option) Redblackmap.dict = Redblackmap.mkDict Utils.same_const_cmp;

Datatype `T1 = C1 num`;

Datatype `R1 = <| f1 : T1 # bool; f2 : num |>`;

fun defcst q =
  let val thm = Define q
  in ((fst o strip_comb o lhs o snd o strip_forall o hd o strip_conj o concl) thm, thm)
  end

fun definv q =
  let val (c,_) = defcst q
  in ((fst o dom_rng o type_of) c, c)
  end

fun test s x f =
  (tprint s;
   case f x of
       NONE => OK()
     | SOME msg => die msg)
  handle e => die (exn_to_string e)

fun matches tgt tm = 
  let val (subst,_) = match_term tgt tm
      fun ok {redex,residue} = is_var redex andalso String.isPrefix "_" (fst (dest_var redex))
  in if List.all ok subst
     then NONE
     else SOME "term doesn't match"
  end

fun no_hyp f ([],tm) = f tm
  | no_hyp f (_:goal) = SOME "goal has unexpected hypotheses"

fun shift_hyp f (hy,tm) = f (list_mk_imp (hy, tm))

fun first f [] = NONE
  | first f (h::t) =
    case f h of
        SOME x => SOME x
      | NONE => first f t

fun matching_goal (h1,t1) (h2,t2) =
  if length h1 <> length h2 then SOME "different number of hypotheses" else
  case first (fn (x,y) => matches x y) (ListPair.zip (h1,h2)) of
      SOME s => SOME "Mismatched hypothesis"
    | NONE => matches t1 t2

fun single_goal f ([gl],_) = f gl
  | single_goal f _ = SOME "wrong number of subgoals"

fun tlist s [] [] = NONE
  | tlist s (f::fs) (h::t) = (case f h of NONE => tlist s fs t | x => x)
  | tlist s _ _ = SOME s

fun goals fs (gls:goal list,_:validation) = tlist "Wrong number of goals" fs gls

fun single f [x] = f x
  | single _ _ = SOME "length <> 1"

val invs = map definv [
  `I1 (t1:T1) = T`,
  `I2 (t1p:T1 # num) = T`,
  `I3 (r:R1) = T`
];

val defns = Redblackmap.fromList Utils.same_const_cmp (map defcst [
  `pairtest a (b,c) (d,e) = C1 (a + b + c + d + e)`,
  `pairinvtest (t1:T1,n:num) = t1`,
  `pairconsttest (t1:T1,b:bool) = t1`,
  `(primrectest 0 = C1 0) /\ (primrectest (SUC n) = primrectest n)`,
  `(primrectest2 n 0 = C1 n) /\ (primrectest2 n (SUC m) = primrectest2 (SUC n) m)`  
]);

val step = VALID (step_tac invs dummy_final defns empty_proofs (K false))

val () = test "Basic pair splitting 1/2"
  (step ([],``I1 (pairtest x y z)``))
  (single_goal (no_hyp (matches ``I1 (pairtest x (_,_) z)``)));
val () = test "Basic pair splitting 2/2"
  (step ([],``I1 (pairtest x (y1,y2) z)``))
  (single_goal (no_hyp (matches ``I1 (pairtest x (y1,y2) (_,_))``)));
val () = test "Pair splitting with repeat"
  (NTAC 2 (step) ([],``I1 (pairtest (FST x) x (f x))``))
  (single_goal (shift_hyp (matches ``(f (_a,_b) = (_c,_d)) ==> I1 (pairtest (FST (_a,_b)) (_a,_b) (_c,_d))``)));
val () = test "Dependent pair 1/2"
  (find_invs_tm invs ``(t1:T1,n:num)``)
  (single (matches ``I2 (t1,n)``))
val () = test "Dependent pair 2/2"
  (find_invs_tm invs ``t1p:T1#num``)
  (single (matches ``I2 t1p``))

val () = test "Pair with invariant unfolding test step 1/3"
  (step ([],``I1 (pairinvtest x)``))
  (goals [no_hyp (matches ``I2 x ==> I1 (pairinvtest x)``),
          no_hyp (matches ``I2 x``)])
val () = test "Pair with invariant unfolding test step 2/3"
  (NTAC 2 (step) ([],``I2 x ==> I1 (pairinvtest x)``))
  (goals [shift_hyp (matches ``I2 (_a,_b) ==> I1 (pairinvtest (_a,_b))``)])
val () = test "Pair with invariant unfolding test step 3/3"
  (step ([``I2 (x,y)``], ``I1 (pairinvtest (x,y))``))
  (goals [shift_hyp (matches ``I2 (_a,_b) ==> I1 _a``)])

val () = test "Pair with constant preserved 1/2"
  (step ([], ``I1 (pairconsttest (x,F))``))
  (goals [no_hyp (matches ``I1 x ==> I1 (pairconsttest (x,F))``),
          no_hyp (matches ``I1 x``)])
val () = test "Pair with constant preserved 2/2"
  (step ([``I1 x``], ``I1 (pairconsttest (x,F))``))
  (goals [shift_hyp (matches ``I1 x ==> I1 x``)])

val () = test "Record update with pair"
  (step ([], ``I3 (r with f1 := t1)``))
  (goals [no_hyp (matches ``I3 r ==> I1 (FST t1) ==> I3 (r with f1 := t1)``),
          no_hyp (matches ``I3 r``),
          no_hyp (matches ``I1 (FST (t1:T1 # bool))``)])

val () = test "Record update with function on pair types"
  (step ([], ``I3 (r with f1 := (f:T1 # num -> T1 # bool) t1)``))
  (goals [no_hyp (matches ``I3 r ==> I1 (FST _t) ==> I3 (r with f1 := _t)``),
          no_hyp (matches ``I3 r``),
          no_hyp (matches ``I1 (FST ((f:T1 # num -> T1 # bool) t1))``)])

val () = test "Trivial use of hypothesis"
  (step ([``I3 r``], ``I3 r``))
  (goals [])

val () = test "Use of hypothesis with implication"
  (step ([``I1 x ==> I3 r``], ``I3 r``))
  (goals [matching_goal ([``I1 x ==> I3 r``], ``I1 x``)])

val () = test "Pair split updates hypothesis"
  (step ([``I2 x``], ``I1 (let (y,z) = (x:T1 # num) in y)``))
  (goals [matching_goal ([``I2 x``], ``I1 (FST (x : T1 # num))``),
          matching_goal ([``I2 (y,z)``], ``I1 y ==> I1 y``)])

val () = test "Record update with hypothesis"
  (step ([``(P : T1 -> bool) (f (x:'a))``], ``I3 (r with f1 := (f (x:'a),b))``))
  (goals [shift_hyp (matches ``P _t ==> I3 r ==> I1 _t ==> I3 (r with f1 := (_t,b))``),
          shift_hyp (matches ``P (_t : T1) ==> I3 r``),
          shift_hyp (matches ``P (f x) ==> I1 (f (x:'a))``)])

val () = test "Option case split updates hypothesis"
  (step ([``x : T1 option = f (y:'a)``], ``I1 (case x of NONE => z | SOME a => a)``))
  (goals [matching_goal ([``NONE : T1 option = f (y : 'a)``], ``I1 z``),
          shift_hyp (matches ``(SOME (_a : T1) = f (y : 'a)) ==>  I1 _a``)])

val () = test "Option case split updates hypothesis 2"
  (step ([``case x of SOME (t:T1) => I1 t | NONE => F``], ``I1 (case x of NONE => z | SOME a => a)``))
  (goals [matching_goal ([``F``], ``I1 z``),
          shift_hyp (matches ``I1 _a ==>  I1 _a``)])

val () = test "Primrec 1"
  (step ([], ``I1 (primrectest n)``))
  (goals [no_hyp (matches ``I1 (C1 0)``),
          shift_hyp (matches ``I1 (primrectest n) ==> I1 (primrectest n)``)])

val () = test "Primrec 2"
  (step ([], ``I1 (primrectest2 n m)``))
  (goals [no_hyp (matches ``I1 (C1 n)``),
          shift_hyp (matches ``I1 (primrectest2 n m) ==> I1 (primrectest2 (SUC n) m)``)])
