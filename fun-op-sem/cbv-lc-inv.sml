open cbvTheory;
open CoreTactics;

val Bounded_def = Define `Bounded s s' = s'.clock <= s.clock`

(* srw_ss as a cheap way to get datatype equations,
   COND_elim_ss to deal with conditionals in clock updates,
   ARITH_ss to deal with comparisons inc +1 *)
fun final_step g _ _ _ =
  (FULL_SIMP_TAC (srw_ss()++boolSimps.COND_elim_ss++numSimps.ARITH_ss) [Bounded_def] THEN NO_TAC) g

val bulksetup : BulkProofTypes.setup = {
  type_invariants = [(``:state``, ``Bounded init_s``)],
  final_step = final_step,
  invariant_breakers = [],
  override_spec = K NONE,
  definitions = Redblackmap.fromList Utils.same_const_cmp [(``dec_clock``, dec_clock_def),
                                                           (``check_clock``,check_clock_def),
                                                           (``sem``,sem_def)],
  special = []
}

val proofs = BulkProof.bulk_prove bulksetup

val sem_bounded = Option.valOf (Redblackmap.find (proofs, ``sem``))

val sem_ok =
  prove
     (``∀env s e r s'. (sem env s e = (r, s')) ⇒ s'.clock ≤ s.clock``,
      REPEAT STRIP_TAC
      THEN ``s' = SND (sem env s e)`` via ASM_SIMP_TAC (srw_ss()) []
      THEN FIRST_ASSUM (fn th => SUBST_TAC [th])
      THEN ASM_SIMP_TAC (srw_ss()++numSimps.ARITH_ss) [
         INST [``init_s:state`` |-> ``s:state``] (SPEC_ALL (REWRITE_RULE [Bounded_def] sem_bounded))
      ])
