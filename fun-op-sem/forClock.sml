open forTheory;

val Bounded_def = Define `Bounded s s' = s'.clock <= s.clock`

(* srw_ss as a cheap way to get datatype equations,
   COND_elim_ss to deal with conditionals in clock updates,
   ARITH_ss to deal with comparisons inc +1 *)
fun final_step g _ _ _ =
  (FULL_SIMP_TAC (srw_ss()++boolSimps.COND_elim_ss++numSimps.ARITH_ss) [Bounded_def] THEN NO_TAC) g

val bulksetup : BulkProofTypes.setup = {
  type_invariants = [(``:state``, ``Bounded init_s``)],
  final_step = final_step,
  invariant_breakers = [],
  override_spec = K NONE,
  definitions = Redblackmap.fromList Utils.same_const_cmp [(``dec_clock``, dec_clock_def),
                                                           (``check_clock``,check_clock_def),
                                                           (``store_var``,store_var_def),
                                                           (``sem_e``,sem_e_def),
                                                           (``sem_t``,sem_t_def)],
  special = []
}

val proofs = BulkProof.bulk_prove bulksetup

(* For manual use *)
val step = BulkProof.mk_step bulksetup proofs;
val manual = BulkProof.manual bulksetup;

val sem_t_bounded = Option.valOf (Redblackmap.find (proofs, ``sem_t``))

val sem_t_ok =
  prove
     (``!s t r s'. (sem_t s t = (r, s')) ⇒ s'.clock ≤ s.clock``,
      REPEAT STRIP_TAC
      THEN ``s' = SND (sem_t s t)`` via ASM_SIMP_TAC (srw_ss()) []
      THEN FIRST_ASSUM (fn th => SUBST_TAC [th])
      THEN ASM_SIMP_TAC (srw_ss()++numSimps.ARITH_ss) [
         INST [``init_s:state`` |-> ``s:state``] (SPEC_ALL (REWRITE_RULE [Bounded_def] sem_t_bounded))
      ])
