(* Start of examples/fun-op-sem/for/forScript.sml, reproduced here
   so that we can use the original sem_t_ind theorem, which is normally
   replaced.

From
commit 49d0d7ba2991d96c1806343205f1d416c3a032e8
Date:   Sun Mar 20 10:38:07 2016 +1100
*)

open HolKernel Parse boolLib bossLib;

val _ = new_theory "for";

(*

This file defines a simple FOR language that's very similar to the FOR
language used by Arthur Charguéraud in his ESOP'13 paper:

  Pretty-Big-Step Semantics
  http://www.chargueraud.org/research/2012/pretty/

This file defines:
 - the syntax of the language,
 - a functional big-step semantics (an interpreter with a clock),
 - a very simple type checker (that is proved sound)
 - a clocked relational big-step semantics (and equivalence proof) and
 - a conventional (unclocked) relational big-step semantics (inductive and co-inductive)
*)

open optionTheory pairTheory pred_setTheory finite_mapTheory stringTheory;
open integerTheory lcsymtacs;

val _ = temp_tight_equality ();

val ect = BasicProvers.EVERY_CASE_TAC;


(* === Syntax === *)

val _ = Datatype `
e = Var string
  | Num int
  | Add e e
  | Assign string e`;

val _ = Datatype `
t =
  | Dec string t
  | Exp e
  | Break
  | Seq t t
  | If e t t
  | For e e t`;

val _ = Datatype `
r = Rval int
  | Rbreak
  | Rtimeout
  | Rfail`;

val r_distinct = fetch "-" "r_distinct";
val r_11 = fetch "-" "r_11";


(* === Functional big-step semantics === *)

val _ = Datatype `
state = <| store : string |-> int; clock : num |>`;

val state_component_equality = fetch "-" "state_component_equality";

val store_var_def = Define `
  store_var v x s = s with store := s.store |+ (v,x)`;

val _ = augment_srw_ss[rewrites[store_var_def]];

val state_rw = Q.prove (
`(!s c. <| store := s; clock := c |>.store = s) ∧
 (!s. <| store := s.store; clock := s.clock |> = s)`,
 rw [state_component_equality]);

(* Expression evaluation *)

val sem_e_def = Define `
(sem_e s (Var x) =
  case FLOOKUP s.store x of
     | NONE => (Rfail, s)
     | SOME n => (Rval n, s)) ∧
(sem_e s (Num num) =
  (Rval num, s)) ∧
(sem_e s (Add e1 e2) =
  case sem_e s e1 of
     | (Rval n1, s1) =>
         (case sem_e s1 e2 of
             | (Rval n2, s2) =>
                 (Rval (n1 + n2), s2)
             | r => r)
     | r => r) ∧
(sem_e s (Assign x e) =
  case sem_e s e of
     | (Rval n1, s1) =>
         (Rval n1, store_var x n1 s1)
     | r => r)`;

(* HOL4's definition requires a little help with the definition. In
   particular, we need to help it see that the clock does not
   decrease. To do this, we add a few redundant checks (check_clock)
   to the definition of the sem_t function. These redundant checks are
   removed later in the script. *)

val sem_e_clock = Q.store_thm ("sem_e_clock",
`!s e r s'. sem_e s e = (r, s') ⇒ s.clock = s'.clock`,
 Induct_on `e` >> rw [sem_e_def] >> ect >>
 fs [] >> rw [] >> metis_tac []);

val sem_e_store = Q.prove (
`!s e r s'. sem_e s e = (r, s') ⇒ FDOM s.store ⊆ FDOM s'.store`,
 Induct_on `e` >> rw [sem_e_def] >> ect >>
 fs [SUBSET_DEF] >> rw [] >> metis_tac []);

val sem_e_res = Q.prove (
`!s e r s'. sem_e s e = (r, s') ⇒ r ≠ Rbreak ∧ r ≠ Rtimeout`,
 Induct_on `e` >> rw [sem_e_def] >> ect >>
 fs [] >> rw [] >> metis_tac []);

val check_clock_def = Define `
check_clock s' s =
  s' with clock := (if s'.clock ≤ s.clock then s'.clock else s.clock)`;

val dec_clock_def = Define `
dec_clock s = s with clock := s.clock - 1`;

val dec_clock_store = Q.store_thm ("dec_clock_store[simp]",
`!s. (dec_clock s).store = s.store`,
 rw [dec_clock_def]);

(* Statement evaluation -- with redundant check_clock *)

val sem_t_def = tDefine "sem_t" `
(sem_t s (Exp e) = sem_e s e) ∧
(sem_t s (Dec x t) =
  sem_t (store_var x 0 s) t) ∧
(sem_t s Break = (Rbreak, s)) ∧
(sem_t s (Seq t1 t2) =
  case sem_t s t1 of
     | (Rval _, s1) =>
         sem_t (check_clock s1 s) t2
     | r => r) ∧
(sem_t s (If e t1 t2) =
  case sem_e s e of
     | (Rval n1, s1) => sem_t s1 (if n1 = 0 then t2 else t1)
     | r => r) ∧
(sem_t s (For e1 e2 t) =
  case sem_e s e1 of
     | (Rval n1, s1) =>
         if n1 = 0 then
           (Rval 0, s1)
         else
           (case sem_t s1 t of
              | (Rval _, s2) =>
                  (case sem_e s2 e2 of
                      | (Rval _, s3) =>
                          if s.clock ≠ 0 ∧ s3.clock ≠ 0 then
                            sem_t (dec_clock (check_clock s3 s)) (For e1 e2 t)
                          else
                            (Rtimeout, s3)
                      | r => r)
              | (Rbreak, s2) =>
                  (Rval 0, s2)
              | r => r)
     | r => r)`
 (WF_REL_TAC `(inv_image (measure I LEX measure t_size)
                            (\(s,t). (s.clock,t)))`
  \\ REPEAT STRIP_TAC \\ TRY DECIDE_TAC
  \\ fs [check_clock_def, dec_clock_def, LET_THM]
  \\ rw []
  \\ imp_res_tac sem_e_clock
  \\ DECIDE_TAC);

val sem_t_clock = Q.store_thm ("sem_t_clock",
`!s t r s'. sem_t s t = (r, s') ⇒ s'.clock ≤ s.clock`,
 ho_match_mp_tac (fetch "-" "sem_t_ind") >>
 reverse (rpt strip_tac) >>
 pop_assum mp_tac >>
 rw [Once sem_t_def] >>
 ect >>
 imp_res_tac sem_e_clock >>
 fs [] >>
 fs [check_clock_def, dec_clock_def, LET_THM] >>
 TRY decide_tac >>
 rfs [] >>
 res_tac >>
 decide_tac);

val check_clock_id = Q.prove (
`!s s'. s.clock ≤ s'.clock ⇒ check_clock s s' = s`,
 rw [check_clock_def, state_component_equality]);

val _ = export_theory ();
