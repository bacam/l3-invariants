(* Start of examples/fun-op-sem/cbv-lc/cbvScript.sml, reproduced here
   so that we can use the original sem_ind theorem, which is normally
   replaced.

From
commit be0ebe36779ec7ff623b596f336f7f71fdcfe6a5
Date:   Fri Mar 18 16:35:37 2016 +1100
 *)

(* An untyped call-by-value lambda calculus with a functional big-step
 * semantics, and a definition of contextual approximation for it. We
 * use closures rather than substitution in the semantics, and use
 * De Bruijn indices for variables. *)

open HolKernel boolLib bossLib lcsymtacs Parse;
open integerTheory stringTheory alistTheory listTheory pred_setTheory;
open pairTheory optionTheory finite_mapTheory arithmeticTheory;

val _ = set_trace "Goalstack.print_goal_at_top" 0;
val _ = ParseExtras.temp_tight_equality();

fun term_rewrite tms = let
  fun f tm = ASSUME (list_mk_forall(free_vars tm,tm))
  in rand o concl o QCONV (REWRITE_CONV (map f tms)) end

val _ = new_theory "cbv";

(* Syntax *)

val _ = Datatype `
lit =
  Int int`;

val _ = Datatype `
exp =
  | Lit lit
  | Var num
  | App exp exp
  | Fun exp
  | Tick exp`;

(* Values *)

val _ = Datatype `
v =
  | Litv lit
  | Clos (v list) exp`;

val v_induction = theorem "v_induction";

val v_ind =
  v_induction
  |> Q.SPECL[`P`,`EVERY P`]
  |> SIMP_RULE (srw_ss()) []
  |> UNDISCH |> CONJUNCT1 |> DISCH_ALL
  |> GEN_ALL
  |> curry save_thm "v_ind";

val v_size_def = definition"v_size_def"

val _ = type_abbrev("env",``:v list``)

(* Semantics *)

(* The state of the semantics will have a clock and a store.
 * Even though we don't have any constructs that access the store, we want the
 * placeholder. *)

val _ = Datatype `
state = <| clock : num; store : v list|>`;

val state_component_equality = theorem "state_component_equality";

val state_clock_idem = Q.store_thm ("state_clock_idem[simp]",
`!s. (s with clock := s.clock) = s`,
 rw [state_component_equality]);

(* machinery for the functional big-step definition *)

val check_clock_def = Define `
  check_clock s' s =
    s' with clock := (if s'.clock ≤ s.clock then s'.clock else s.clock)`;

val check_clock_id = prove(
  ``!s s'. s.clock ≤ s'.clock ⇒ check_clock s s' = s``,
 rw [check_clock_def, state_component_equality]);

val dec_clock_def = Define `
  dec_clock s = s with clock := s.clock - 1`;

(* results *)

val _ = Datatype`
  r = Rval v
    | Rfail
    | Rtimeout`;

(* big-step semantics as a function *)

val sem_def = tDefine "sem" `
(sem env s (Lit i) = (Rval (Litv i), s)) ∧
(sem env s (Var n) =
  if n < LENGTH env then
    (Rval (EL n env), s)
  else
    (Rfail, s)) ∧
(sem env s (App e1 e2) =
 case sem env s e1 of
 | (Rval v1, s1) =>
     (case sem env (check_clock s1 s) e2 of
      | (Rval v2, s2) =>
          if s.clock ≠ 0 ∧ s2.clock ≠ 0 then
            (case v1 of
             | Clos env' e =>
               sem (v2::env') (dec_clock (check_clock s2 s)) e
             | _ => (Rfail, s2))
          else
            (Rtimeout, s2)
      | res => res)
 | res => res) ∧
(sem env s (Fun e) = (Rval (Clos env e), s)) ∧
(sem env s (Tick e) =
  if s.clock ≠ 0 then
    sem env (dec_clock s) e
  else
    (Rtimeout, s))`
(WF_REL_TAC`inv_image (measure I LEX measure exp_size)
                      (λ(env,s,e). (s.clock,e))` >>
 rpt strip_tac >> TRY DECIDE_TAC >>
 fs[check_clock_def,dec_clock_def] >>
 rw[] >> fsrw_tac[ARITH_ss][]);

val _ = export_theory ();
