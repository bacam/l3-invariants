open BulkProof;

val proofs = bulk_prove Model.default_invariant_setup;
report proofs;

(*

For manual use:

open CoreTactics Model;
val step = BulkProof.mk_step Model.default_invariant_setup proofs;
val manual = BulkProof.manual Model.default_invariant_setup;

fun step' c = BulkProof.mk_step Model.default_invariant_setup (fst (Redblackmap.remove (proofs,c)))

*)
