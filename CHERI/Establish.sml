open CoreTactics;

val defns = Utils.thy_defns Model.thy (#C Model.inventory)


val memok =
    Tactical.prove
       (``MemInv ((InitMEM s).the_MEM)``,
        SIMP_TAC (srw_ss()) [Model.MemInv_def,Model.DataTypeInv_def,cheriTheory.InitMEM_def])

val defCap_ok =
    Tactical.prove
       (``CapInv defaultCap``,
        SIMP_TAC (srw_ss()) [cheriTheory.defaultCap_def,boolTheory.LET_THM,Model.CapInv_def])


val CoreInvariant_def =
  Define `CoreInvariant i s =
            (CapFileInv (s.c_capr i)) /\
            (let c = s.c_pcc i in CapInv c) /\
            (BranchSlotInv (s.c_BranchDelayPCC i)) /\
            (BranchSlotInv (s.c_BranchToPCC i))`


val CapFileCur = Define `CapFileCur s = CapFileInv (s.c_capr s.procID) /\ CapInv (s.c_pcc s.procID)`

val capfile_init =
    Tactical.prove
       (``CapFileCur (COP2Init () s)``,
        ASM_SIMP_TAC (srw_ss()) [boolTheory.LET_THM,
                                 cheriTheory.COP2Init_def,
                                 cheriTheory.write'CAPR_def,
                                 cheriTheory.defaultCap_def,
                                 cheriTheory.write'PCC_def,
                                 cheriTheory.write'capcause_def]
        THEN Cases_on `2 <= s.trace_level`
        THEN NTAC 32 (
           ONCE_REWRITE_TAC [state_transformerTheory.FOR_def]
           THEN ASM_SIMP_TAC (srw_ss())
             [state_transformerTheory.BIND_DEF,
              combinTheory.APPLY_UPDATE_THM,
              combinTheory.UPDATE_EQ])
        THEN ASM_SIMP_TAC (srw_ss()) [CapFileCur,Model.CapFileInv_def,Model.CapInv_def]
        THEN (CONJ_TAC
              THENL [wordsLib.Cases_word_value THEN EVAL_TAC, EVAL_TAC]))

val cop2init_procid =
    Tactical.prove 
       (``(COP2Init () s).procID = s.procID``,
        ASM_SIMP_TAC (srw_ss()) [boolTheory.LET_THM,
                                 cheriTheory.COP2Init_def,
                                 cheriTheory.write'CAPR_def,
                                 cheriTheory.defaultCap_def,
                                 cheriTheory.write'PCC_def,
                                 cheriTheory.write'capcause_def]
        THEN Cases_on `2 <= s.trace_level`
        THEN NTAC 32 (
           ONCE_REWRITE_TAC [state_transformerTheory.FOR_def]
           THEN ASM_SIMP_TAC (srw_ss())
             [state_transformerTheory.BIND_DEF,
              combinTheory.APPLY_UPDATE_THM,
              combinTheory.UPDATE_EQ])
        THEN ASM_SIMP_TAC (srw_ss()) [])

val capfilepreserved =
    Tactical.prove
       (``CapFileInv (s.c_capr i) /\ CapInv (s.c_pcc i) ==>
          CapFileInv ((COP2Init () s).c_capr i) /\ CapInv (s.c_pcc i)``,
        Cases_on `i = s.procID` THENL
        [STRIP_TAC
         THEN ASM_SIMP_TAC std_ss [REWRITE_RULE [CapFileCur,cop2init_procid] capfile_init],

         ASM_SIMP_TAC (srw_ss()) [boolTheory.LET_THM,
                                  cheriTheory.COP2Init_def,
                                  cheriTheory.write'CAPR_def,
                                  cheriTheory.defaultCap_def,
                                  cheriTheory.write'PCC_def,
                                  cheriTheory.write'capcause_def]
         THEN Cases_on `2 <= s.trace_level`
         THEN NTAC 32 (
            ONCE_REWRITE_TAC [state_transformerTheory.FOR_def]
            THEN ASM_SIMP_TAC (srw_ss())
              [state_transformerTheory.BIND_DEF,
               combinTheory.APPLY_UPDATE_THM,
               combinTheory.UPDATE_EQ])
         THEN ASM_SIMP_TAC (srw_ss()) [Model.CapFileInv_def,Model.CapInv_def]])


val procid_pres =
    map Tactical.prove
        [(``(write'CP0 (f v) s).procID = s.procID``,
          SIMP_TAC (srw_ss()) [cheriTheory.write'CP0_def]),
         (``(JTAG_UART_initialise uart s).procID = s.procID``,
          SIMP_TAC (srw_ss()) [cheriTheory.JTAG_UART_initialise_def,
                               boolTheory.LET_THM])]

(* The rewrite/beta is a bit imprecise *)
fun spec_let_tac (asl,w) =
    let val tm = find_term is_let w
        val (v,m,n) = pairSyntax.dest_plet tm
        val v' = variant (free_vars w) v
    in (SPEC_TAC (m,v') THEN STRIP_TAC THEN ONCE_REWRITE_TAC [boolTheory.LET_THM] THEN BETA_TAC) (asl,w)
    end

val SameProcID_def = Define `SameProcID s s' = (s'.procID = s.procID)`

(* Setup for next result;  note that init_s has to be distinct from variables that
   appear in definitions because the tactic doesn't handle some clashes *)
val procid_step = step_tac [(``:cheri_state``, ``SameProcID init_s``)]
                           (#final_step Model.default_invariant_setup)
                           defns
                           (Redblackmap.mkDict Utils.same_const_cmp)
                           (K false)

val initMipsPreservesProcID =
    Tactical.prove
       (``SameProcID init_s (initMips (pc,uart,extra) init_s)``,
        REPEAT procid_step THEN FULL_SIMP_TAC (srw_ss()) [SameProcID_def])

val preserve_step = step_tac [(``:cheri_state``, ``CoreInvariant i``)] 
                             (#final_step Model.default_invariant_setup)
                             defns
                             (Redblackmap.mkDict Utils.same_const_cmp)
                             (K false)

val InitConds_def = Define `InitConds i s = 
       (s.procID = i) /\
       (BranchSlotInv (s.c_BranchDelayPCC s.procID)) /\
       (BranchSlotInv (s.c_BranchToPCC s.procID))`

val init_step = step_tac [(``:cheri_state``, ``InitConds init_s.procID``)] 
                         (#final_step Model.default_invariant_setup)
                         defns
                         (Redblackmap.mkDict Utils.same_const_cmp)
                         (Utils.term_matches_pat `LET _ (SND (FOR _ _))`)

val est_step = step_tac [(``:cheri_state``, ``CoreInvariant init_s.procID``)] 
                        (#final_step Model.default_invariant_setup)
                        defns
                        (Redblackmap.mkDict Utils.same_const_cmp)
                        (K false)

fun THEN1STEP (tac1,tac2) gl =
    let val (gls, jf) = tac1 gl
    in case gls of
           [] => failwith "First tac in THEN1STEP produced no subgoals"
         | gl'::t =>
           let val (gls', jf') = tac2 gl'
               val len = length gls'
           in (gls'@t, fn ths => jf (jf' (List.take (ths,len)) :: (List.drop (ths,len))))
           end
    end

infix THEN1STEP;

val establish =
    Tactical.prove
       (``InitConds init_s.procID init_s ==>
          CoreInvariant init_s.procID (initMips (pc,uart,extra) init_s)``,

        REPEAT init_step THEN
        TRY (FULL_SIMP_TAC (srw_ss()++boolSimps.CONJ_ss) [InitConds_def]
             THEN NO_TAC)
        THEN est_step

        THENL
        [ (fn (gl as (_,g)) =>
              let val tm = (find_term (Utils.term_matches_pat `write'PC x s`) g)
              in (SUFF_TAC ``InitConds init_s.procID ^tm`` 
                  THEN1STEP SPEC_TAC (tm,``s1:cheri_state``)) gl
              end) THENL
          [ REPEAT STRIP_TAC
            THEN est_step
            THEN est_step THENL
            [ est_step THEN1STEP SIMP_TAC (srw_ss()) [CoreInvariant_def] THEN
              SUFF_TAC ``InitConds init_s.procID (COP2Init () s1) /\
                         CapFileCur (COP2Init () s1)`` THENL
              [ SIMP_TAC (srw_ss ()++boolSimps.CONJ_ss)
                         [InitConds_def,CapFileCur,CoreInvariant_def,boolTheory.LET_THM],
                CONJ_TAC THENL
                [ REPEAT init_step THEN
                  TRY (FULL_SIMP_TAC (srw_ss()++boolSimps.CONJ_ss) [InitConds_def]
                       THEN NO_TAC),

                  MATCH_ACCEPT_TAC capfile_init
                ]
              ],

              REPEAT est_step THEN
              TRY (FULL_SIMP_TAC (srw_ss()++boolSimps.CONJ_ss) [CoreInvariant_def]
                   THEN NO_TAC)
            ],

            REPEAT init_step THEN
            TRY (FULL_SIMP_TAC (srw_ss()++boolSimps.CONJ_ss) [InitConds_def]
                 THEN NO_TAC)
          ],
          REPEAT est_step THEN
          TRY (FULL_SIMP_TAC (srw_ss()++boolSimps.CONJ_ss) [CoreInvariant_def]
               THEN NO_TAC)
        ])

val CoreInvOther_def = Define`CoreInvOther s = !i. i <+ s.procID ==> CoreInvariant i s`

val preserve_other_step = step_tac [(``:cheri_state``, ``CoreInvOther``)] 
                                   (#final_step Model.default_invariant_setup)
                                   defns
                                   (Redblackmap.fromList Utils.same_const_cmp [])
                                   (K false)
val preserve =
    Tactical.prove
       (``CoreInvOther s ==>
          CoreInvOther (initMips (pc,uart,extra) s)``,
        REPEAT preserve_other_step THEN
        TRY (FULL_SIMP_TAC (srw_ss()) [CoreInvOther_def,
                                       CoreInvariant_def,
                                       boolTheory.LET_THM,
                                       combinTheory.UPDATE_APPLY,
                                       wordsTheory.WORD_LOWER_NOT_EQ] THEN NO_TAC))

val FOR_right =
    Tactical.prove
       (``!j i. (FOR (i,i+SUC j,a) = BIND (FOR (i,i+j,a)) (\_. a (i+SUC j)))``,
        Induct THEN STRIP_TAC THENL
        [ ONCE_REWRITE_TAC [state_transformerTheory.FOR_def]
          THEN SIMP_TAC arith_ss []
          THEN ONCE_REWRITE_TAC [state_transformerTheory.FOR_def]
          THEN SIMP_TAC arith_ss []
        ,
          ONCE_REWRITE_TAC [state_transformerTheory.FOR_def]
          THEN SIMP_TAC arith_ss []
          THEN REWRITE_TAC [GSYM state_transformerTheory.BIND_ASSOC] 
          THEN AP_TERM_TAC
          THEN ABS_TAC
          THEN ONCE_REWRITE_TAC [DECIDE ``i + SUC x = (i + 1) + x``]
          THEN ASM_SIMP_TAC arith_ss []
        ])

val FOR_right0 = GEN_ALL (SIMP_RULE arith_ss [] (SPECL [``n:num``,``0:num``] FOR_right))

val MemInit_def = Define `MemInit s = MemInv s.the_MEM`

val procid0_def = Define `ProcID0 s = (s.procID = 0w)`
val procid0_step = step_tac [(``:cheri_state``, ``ProcID0``)] 
                            (#final_step Model.default_invariant_setup)
                            defns
                            (Redblackmap.mkDict Utils.same_const_cmp)
                            (K false)

val est_mem =
    Tactical.prove
       (``ProcID0 s ==> MemInit (initMips (pc,uart,rdhwr_extra) s)``,
        REPEAT procid0_step
        THEN FULL_SIMP_TAC (srw_ss()) [procid0_def,MemInit_def,Model.MemInv_def,Model.DataTypeInv_def])

val meminit_step = step_tac [(``:cheri_state``, ``MemInit``)] 
                            (#final_step Model.default_invariant_setup)
                            defns
                            (Redblackmap.mkDict Utils.same_const_cmp)
                            (K false)

val pres_mem =
    Tactical.prove
       (``MemInit s ==> MemInit (initMips (pc,uart,rdhwr_extra) (SND (switchCore n s)))``,
        REPEAT meminit_step
        THEN FULL_SIMP_TAC (srw_ss()) [procid0_def,MemInit_def,Model.MemInv_def,Model.DataTypeInv_def])

val non_zero_suc =
    Tactical.prove
       (``!n. 0 < n ==> ?m. n = SUC m``,
        Cases THEN SIMP_TAC arith_ss [])

val switch_n =
    Tactical.prove
       (``!n. (SND (switchCore n s)).procID = n2w n``,
        STRIP_TAC THEN
        SIMP_TAC std_ss [state_transformerTheory.BIND_DEF,
                         cheriTheory.switchCore_def,
                         pairTheory.UNCURRY,
                         boolTheory.LET_THM]
        THEN Cases_on `n = w2n s.procID`
        THEN ASM_SIMP_TAC (srw_ss()) [])

(* *** The extra assumptions that we make about the initial state.
   We need to assume that the capability branch delay information is valid
   because there isn't any code in COP2Init to initialise them.  Fortunately
   the SML simulator uses a default value of NONE anyway.

   We probably should fix this in the model, but I wanted to perform an
   exercise in verifying the model as-is. *)

val Precondition_def =
    Define
       `Precondition s =
          (!i. BranchSlotInv (s.c_BranchDelayPCC i)) /\
          (!i. BranchSlotInv (s.c_BranchToPCC i))`

val switch_precond =
    Tactical.prove
       (``!n s. Precondition s ==> Precondition (SND (switchCore n s))``,
        REPEAT STRIP_TAC THEN
        SIMP_TAC std_ss [state_transformerTheory.BIND_DEF,
                         cheriTheory.switchCore_def,
                         pairTheory.UNCURRY,
                         boolTheory.LET_THM]
        THEN Cases_on `n = w2n s.procID`
        THEN FULL_SIMP_TAC (srw_ss()) [Precondition_def])

val switch_coreinv =
    Tactical.prove
       (``!n s. CoreInvariant i s ==> CoreInvariant i (SND (switchCore n s))``,
        REPEAT STRIP_TAC THEN
        SIMP_TAC std_ss [state_transformerTheory.BIND_DEF,
                         cheriTheory.switchCore_def,
                         pairTheory.UNCURRY,
                         boolTheory.LET_THM]
        THEN Cases_on `n = w2n s.procID`
        THEN FULL_SIMP_TAC (srw_ss()) [CoreInvariant_def])

val precond_step = step_tac [(``:cheri_state``, ``Precondition``)] 
                            (#final_step Model.default_invariant_setup)
                            defns
                            (Redblackmap.mkDict Utils.same_const_cmp)
                            (K false)

val pres_precond =
    Tactical.prove
       (``Precondition s ==> Precondition (initMips (pc,uart,rdhwr_extra) (SND (switchCore i s)))``,
        REPEAT precond_step
        THEN FULL_SIMP_TAC (srw_ss()) [Precondition_def])

val StepInvDef =
    Define
       `StepInv cores s =
          Precondition s /\
          w2n s.procID < cores /\
          MemInv s.the_MEM /\
          !i. w2n i < cores ==> CoreInvariant i s`

val step_ok =
    Tactical.prove
       (``Precondition s ==>
          CoreInvariant (n2w i) (initMips (pc,uart,rdhwr_extra) (SND (switchCore i s)))``,
        REWRITE_TAC [GSYM switch_n]
        THEN STRIP_TAC
        THEN MATCH_MP_TAC (GEN_ALL establish)
        THEN KNOW_TAC ``Precondition (SND (switchCore i s))`` THEN1 ASM_SIMP_TAC std_ss [switch_precond]
        THEN SIMP_TAC (srw_ss()++boolSimps.CONJ_ss) [InitConds_def,Precondition_def])

val LO_N2W_W2N =
    Tactical.prove
       (``!w : 'a word. !n. w <+ n2w n ==> w2n w < n``,
        REWRITE_TAC [wordsTheory.WORD_LO,wordsTheory.w2n_n2w]
        THEN REPEAT STRIP_TAC
        THEN MATCH_MP_TAC (SPECL [``w2n (w:'a word)``,
                                  ``n MOD dimword (:'a)``,
                                  ``n:num``] arithmeticTheory.LESS_LESS_EQ_TRANS)
        THEN ASM_SIMP_TAC arith_ss [arithmeticTheory.MOD_LESS_EQ,wordsTheory.ZERO_LT_dimword])


val W2N_LO_N2W =
    Tactical.prove
       (``!m. !w:'a word. m < dimword (:'a) ==> w2n w < m ==> w <+ n2w m``,
        REWRITE_TAC [wordsTheory.WORD_LO,wordsTheory.w2n_n2w]
        THEN SIMP_TAC arith_ss [])

(* *** The main theorem.

   Given a reasonable number of cores, any preconditions missing from the model
   (defined above), then initialising the model will establish the invariant in
   CHERI-Model.sml.

   Note that the HOL code below is intended to mimic the initialisation code in
   the SML simulator. *)

val invariant_established =
    Tactical.prove
       (``!cores s pc uart rdhwr_extra.
          0 < cores ==>
          cores < 256 ==>
          Precondition s ==>
          ModelInvariant 
          let s = SND (FOR (0,cores-1,\i. BIND (switchCore i)
                                               (\_. \s. ((), initMips (pc, (uart, rdhwr_extra)) s))) s)
          in s with totalCore := cores``,

        REPEAT STRIP_TAC
        THEN IMP_RES_TAC non_zero_suc
        THEN ASM_SIMP_TAC arith_ss []
        THEN KNOW_TAC ``m:num < 256`` THEN1 DECIDE_TAC
        THEN REPEAT (WEAKEN_TAC (Lib.can (find_term (term_eq ``cores:num``))))
        THEN STRIP_TAC

        THEN
        SUFF_TAC ``StepInv (SUC m)
                           (SND (FOR (0,m,\i. BIND (switchCore i)
                                                   (\_. \s. ((), initMips (pc, (uart, rdhwr_extra)) s))) s))``
          THEN1
            SIMP_TAC (srw_ss()++boolSimps.CONJ_ss) [boolTheory.LET_THM,
                                                    StepInvDef,
                                                    CoreInvariant_def,
                                                    Model.ModelInvariant_def]

        THEN Induct_on `m`
        THENL
        [
          ONCE_REWRITE_TAC [state_transformerTheory.FOR_def]
          THEN SIMP_TAC arith_ss [StepInvDef,state_transformerTheory.BIND_DEF,pairTheory.UNCURRY]
          THEN REPEAT CONJ_TAC
          THENL
          [ ASM_SIMP_TAC std_ss [pres_precond] 
          ,
            REWRITE_TAC [REWRITE_RULE [SameProcID_def] initMipsPreservesProcID]
            THEN SIMP_TAC (arith_ss++wordsLib.WORD_ss) [switch_n]
          ,
            MATCH_MP_TAC (REWRITE_RULE [procid0_def,MemInit_def] est_mem)
            THEN SIMP_TAC arith_ss [switch_n]
          ,
            REPEAT STRIP_TAC
            THEN KNOW_TAC ``i = 0w:word8`` THEN1 wordsLib.WORD_DECIDE_TAC
            THEN DISCH_THEN SUBST1_TAC
            THEN ASM_SIMP_TAC std_ss [step_ok]
          ],

          STRIP_TAC
          THEN REWRITE_TAC [FOR_right0]
          THEN FULL_SIMP_TAC std_ss [state_transformerTheory.BIND_DEF,pairTheory.UNCURRY,StepInvDef]
          THEN REPEAT CONJ_TAC
          THENL
          [ ASM_SIMP_TAC arith_ss [pres_precond]
          ,
            REWRITE_TAC [REWRITE_RULE [SameProcID_def] initMipsPreservesProcID]
            THEN ASM_SIMP_TAC (arith_ss++wordsLib.WORD_ss) [switch_n,wordsTheory.w2n_n2w]
          ,
            ASM_SIMP_TAC arith_ss [REWRITE_RULE [MemInit_def] pres_mem]
          ,
            REPEAT STRIP_TAC
            THEN Cases_on `w2n i = SUC m`
            THENL
            [ KNOW_TAC ``i : word8 = n2w (SUC m)`` THEN1
                FIRST_ASSUM (fn th => ACCEPT_TAC (REWRITE_RULE [wordsTheory.n2w_w2n]
                                                               (AP_TERM ``n2w : num -> word8`` th)))
              THEN DISCH_THEN SUBST1_TAC
              THEN ASM_SIMP_TAC arith_ss [step_ok]
            ,
    

              MATCH_MP_TAC (MP_CANON (REWRITE_RULE [CoreInvOther_def] preserve))
              THEN ASM_SIMP_TAC arith_ss [REWRITE_RULE [SameProcID_def] initMipsPreservesProcID,switch_n]
              THEN REPEAT STRIP_TAC
              THENL
              [ MATCH_MP_TAC switch_coreinv
                THEN KNOW_TAC ``w2n (i':word8) < SUC m``
                  THEN1 ASM_SIMP_TAC (arith_ss++wordsLib.WORD_ss) [LO_N2W_W2N]
                THEN ASM_SIMP_TAC arith_ss []
              ,
                ASM_SIMP_TAC (arith_ss++wordsLib.WORD_ss) [W2N_LO_N2W]
              ]
            ]
          ]
        ])
