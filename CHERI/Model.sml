structure Model : sig
  include Model
  val CapInv_def : thm
  val CapFileInv_def : thm
  val DataTypeInv_def : thm
  val MemInv_def : thm
  val BranchSlotInv_def : thm
  val ModelInvariant_def : thm
  val invariant_defs : thm list
  val CapExInv_def : thm
end =
struct

open HolKernel Parse boolLib bossLib;

(* Hide annoying constants *)
val _ = hide "c"
val _ = hide "i"
val _ = hide "r"

val CapInv_def =
  Define `CapInv c =
          c.tag ==>
            ((w2w c.base + w2w c.length <+ 0x10000000000000000w :65 word) /\
            ((¬ c.sealed) ==> (c.otype = 0w)) /\
            (c.reserved = 0w))`

val CapFileInv_def =
  Define `CapFileInv (cs:word5 -> Capability) = !j. let c = cs j in CapInv c`

val DataTypeInv_def =
  Define `DataTypeInv d = case d of Cap c => CapInv c | Raw _ => T`

val MemInv_def =
  Define `MemInv (m:35 word -> DataType) = !j. DataTypeInv (m j)`

val BranchSlotInv_def =
  Define `BranchSlotInv (slot : (word64 # Capability) option) =
            (case slot of SOME c => CapInv (SND c) | NONE => T)`

val ModelInvariant_def =
  Define `ModelInvariant s =
            (MemInv s.the_MEM) /\
            w2n s.procID < s.totalCore /\
            !i. w2n i < s.totalCore ==>
            (CapFileInv (s.c_capr i)) /\
            (let c = s.c_pcc i in CapInv c) /\
            (BranchSlotInv (s.c_BranchDelayPCC i)) /\
            (BranchSlotInv (s.c_BranchToPCC i))`;

(* Special form for LoadCap, which may return ARB when an exception is signalled *)
val CapExInv_def =
    Define `CapExInv cs = (~ (SND cs).c_state.c_exceptionSignalled ==> CapInv (FST cs))`

val invariant_defs = [ModelInvariant_def,CapFileInv_def,DataTypeInv_def,MemInv_def,CapInv_def,BranchSlotInv_def,CapExInv_def]

val type_invariants = [
  (``:Capability``, ``CapInv``),
  (``:word5 -> Capability``, ``CapFileInv``),
  (``:DataType``, ``DataTypeInv``),
  (``:cheri_state``, ``ModelInvariant``),
  (``:(word64 # Capability) option``, ``BranchSlotInv``)
]

val tag0 = Tactical.prove
  (``word_bit 256 ((0w : 1 word @@ w : 256 word): 257 word) = F``,
   wordsLib.WORD_DECIDE_TAC)

val mem_update = Tactical.prove
  (``MemInv m ==> DataTypeInv d ==> MemInv ((a =+ d) m)``,
   SIMP_TAC (srw_ss()) [MemInv_def,DataTypeInv_def]
   THEN REPEAT STRIP_TAC
   THEN Cases_on `a = j`
   THEN ASM_SIMP_TAC (srw_ss()) [combinTheory.UPDATE_APPLY])

val mem_update' = REWRITE_RULE invariant_defs mem_update

val capfile_update = Tactical.prove
  (``!c f r. CapInv c /\ CapFileInv f ==> CapFileInv ((r =+ c) f)``,
   SIMP_TAC std_ss [CapFileInv_def,boolTheory.LET_THM]
   THEN REPEAT STRIP_TAC
   THEN Cases_on `r = j`
   THEN ASM_SIMP_TAC std_ss [combinTheory.UPDATE_APPLY])

val simp =
    FULL_SIMP_TAC (srw_ss()) (mem_update'::tag0::boolTheory.LET_THM::combinTheory.UPDATE_APPLY::invariant_defs)

fun final_step gl _ _ t =
    if type_of t = ``:cheri_state`` then
       let val v = genvar ``:word8`` in
          (REWRITE_TAC [ModelInvariant_def]
           THEN (CONJ_TAC THENL [ALL_TAC, CONJ_TAC THENL [ALL_TAC, X_GEN_TAC v THEN Cases_on `^v = ^t.procID`]])
           THEN simp THEN NO_TAC) gl
       end
    else if type_of t = ``:word5 -> Capability`` then
       (FIRST 
        [MATCH_MP_TAC capfile_update
         THEN simp,
         REWRITE_TAC [CapFileInv_def]
         THEN wordsLib.Cases_word_value
         THEN simp] THEN NO_TAC) gl
    else (simp THEN NO_TAC) gl

val state_ty = ``:cheri_state``

val thy = "cheri"

val inventory = cheriTheory.inventory

val invariant_breakers = [
   "setTag",
   "setSealed",
   "setBounds",
   "setType",
   "rec'Capability",
   "write'reg'Capability"
]

(* Semi-automatic cases *)

(* For csetlen, but more general... *)
fun let_intro_in_tac tm tg gl =
  let val v = pairSyntax.genvarstruct (type_of tm)
      val ltm = pairSyntax.mk_plet (v,tm,subst [tm |-> v] tg)
      val th = SYM ((ONCE_REWRITE_CONV [boolTheory.LET_THM]
                     THENC PairRules.PBETA_CONV) ltm)
  in ONCE_REWRITE_TAC [th] gl
  end
fun let_intro_tac tm gl = let_intro_in_tac tm (snd gl) gl
fun state_and_tac (tac : term -> tactic) (hs,tm) =
  tac (find_term (fn tm => is_var tm andalso type_of tm = ``:cheri_state``) tm) (hs,tm)

fun loadcap_tac proofs (hs,tm) =
  let val lcth = Option.valOf (Redblackmap.find (proofs,``LoadCap``))
      val (subs,_) = match_term ``LoadCap x s`` (rand (rand tm))
      val th = INST subs lcth
  in FIRST_ASSUM (fn th' => ASSUME_TAC (MP th th')) (hs,tm)
  end

fun override_spec c =
    Lib.total (Lib.assoc c)
    [(``LoadCap``,
      ``ModelInvariant s ==>
        CapExInv (LoadCap x s) /\
        ModelInvariant (SND (LoadCap x s))``),
     (``switchCore``,
      ``n < s.totalCore ==>
        ModelInvariant s ==>
        ModelInvariant (SND (switchCore n s))``)]

val special : (term * ((term, thm option) Redblackmap.dict *
                       ((term -> bool) -> tactic) -> tactic)) list = 
 [(``defaultCap``, fn (_,step) =>
                      REWRITE_TAC [cheriTheory.defaultCap_def] THEN
                      SIMP_TAC (srw_ss()) [boolTheory.LET_THM] THEN
                      REPEAT (step (K false))),
  (``nullCap``, fn (_,step) =>
                    REWRITE_TAC [cheriTheory.nullCap_def] THEN
                    SIMP_TAC (srw_ss()) [boolTheory.LET_THM] THEN
                    REPEAT (step (K false))),
  (``LoadCap``, fn (_,step) =>
                   REPEAT (step (K false))
                   THEN FULL_SIMP_TAC std_ss [CapExInv_def,cheriTheory.exceptionSignalled_def]),
  (``dfn'CUnseal``, fn (_,step) =>
                       REPEAT (step (Utils.term_matches_pat `setType _`))
                       THEN SIMP_TAC (srw_ss()) [cheriTheory.setType_def,
                                                 cheriTheory.setSealed_def]
                       THEN REPEAT (step (K false))),
  (* For the next instruction we stop before the checks, pull out
     the accesses to the state to get the capability, show that capability
     satisfies CapInv, then squash the computation down so that when we process
     the conditions we don't forget which capability we have a hypothesis for. *)
  (``dfn'CSetBounds``, fn (_,step) =>
                          REPEAT (step (Utils.term_matches_pat `LET _ (_:word64)`))
                          THEN SIMP_TAC std_ss [boolTheory.LET_THM]
                          THEN state_and_tac (fn s => fn (h,t) =>
                                                 let_intro_in_tac ``CAPR q' ^s`` (rand t) (h,t))
                          THEN NTAC 4 (step (K false))
                          THEN SIMP_TAC (srw_ss()) [boolTheory.LET_THM,
                                                    cheriTheory.CAPR_def,
                                                    cheriTheory.setBounds_def,
                                                    cheriTheory.getTag_def,
                                                    cheriTheory.getBase_def,
                                                    cheriTheory.getOffset_def,
                                                    cheriTheory.getLength_def]
                          THEN REPEAT (step (K false))
                          THEN simp
                          (* expensive! *)
                          THEN blastLib.FULL_BBLAST_TAC),
  (``dfn'CSeal``, fn (_,step) =>
                        REPEAT (step is_cond)
                        THEN REPEAT (step (not o is_cond) THEN1 (REPEAT (step (K false))))
                        THEN FULL_SIMP_TAC (srw_ss()) [boolTheory.LET_THM,
                                                       cheriTheory.CAPR_def,
                                                       cheriTheory.getBase_def,
                                                       cheriTheory.getOffset_def,
                                                       cheriTheory.setType_def,
                                                       cheriTheory.setSealed_def]
                        THEN NTAC 3 (step (K false))
                        THEN simp),
  (``dfn'CJR``, fn (_,step) =>
                   REPEAT (step (K false))
                   THEN REWRITE_TAC [BranchSlotInv_def]
                   THEN SIMP_TAC std_ss []
                   THEN REPEAT (step (K false))),
  (``dfn'CJALR``, fn (_,step) =>
                   REPEAT (step (K false))
                   THEN REWRITE_TAC [BranchSlotInv_def]
                   THEN SIMP_TAC std_ss []
                   THEN REPEAT (step (K false))),
  (``dfn'CLC``, fn (proofs,step) =>
                   REPEAT (step (Utils.term_matches_pat `LET _ (LoadCap _ _)`))
                   THEN ONCE_REWRITE_TAC [boolTheory.LET_THM]
                   THEN CONV_TAC (RAND_CONV PairRules.PBETA_CONV)
                   THEN REPEAT (step (K false))
                   THEN loadcap_tac proofs
                   THEN FULL_SIMP_TAC std_ss [cheriTheory.exceptionSignalled_def,CapExInv_def]),
  (``dfn'CLLC``, fn (proofs,step) =>
                   REPEAT (step (Utils.term_matches_pat `LET _ (LoadCap _ _)`))
                   THEN ONCE_REWRITE_TAC [boolTheory.LET_THM]
                   THEN CONV_TAC (RAND_CONV PairRules.PBETA_CONV)
                   THEN REPEAT (step (K false))
                   THEN loadcap_tac proofs
                   THEN FULL_SIMP_TAC std_ss [cheriTheory.exceptionSignalled_def,CapExInv_def]),
  (``switchCore``, fn (_,step) =>
                      REPEAT (step (Utils.term_matches_pat `LET _ (i:word8)`))
                      THEN SIMP_TAC (srw_ss()) [boolTheory.LET_THM]
                      THEN simp
                      THEN UNDISCH_TAC ``n < s.totalCore``
                      THEN Omega.OMEGA_TAC)
 ];

val default_invariant_setup = {
   type_invariants = type_invariants,
   final_step = final_step,
   invariant_breakers = invariant_breakers,
   override_spec = override_spec,
   definitions = Utils.thy_defns thy (#C inventory),
   special = special
}

(*

fun er tac =
  let fun aux p =
    case (SOME (e tac)) handle _ => NONE of
      NONE => p
    | SOME p' => aux p'
  in case (SOME (e tac)) handle _ => NONE of
       NONE => e ALL_TAC
     | SOME p => aux p
  end

val step = fn stop => step_tac (#type_invariants default_invariant_setup) (#final_step default_invariant_setup) proofs stop

*)

end
