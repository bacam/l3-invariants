signature Model =
sig

include Abbrev

(* Details of the model *)

(* Theory name *)
val thy : string
(* Inventory for the model *)
val inventory : {Thy: string, T: string list, C: string list, N: int list}

val default_invariant_setup : BulkProofTypes.setup

end
