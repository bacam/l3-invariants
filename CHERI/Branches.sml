structure CHERIBranches =
struct

local
open HolKernel Parse boolLib bossLib;
in

(* Hide annoying constants *)
val _ = hide "c"
val _ = hide "i"
val _ = hide "r"

val Empty_def =
  Define `Empty s =
            (s.c_state.c_BranchTo = NONE) /\
            (s.c_BranchToPCC s.procID = NONE) /\
            (s.c_state.c_BranchDelay = NONE) /\
            (s.c_BranchDelayPCC s.procID = NONE)`

val OneTo_def =
  Define `OneTo s =
            (s.c_state.c_BranchTo <> NONE ==> (s.c_BranchToPCC s.procID = NONE)) /\
            (s.c_BranchToPCC s.procID <> NONE ==> (s.c_state.c_BranchTo = NONE)) /\
            (s.c_state.c_BranchDelay = NONE) /\
            (s.c_BranchDelayPCC s.procID = NONE)`
val OneDelay_def =
  Define `OneDelay s =
            (s.c_state.c_BranchTo = NONE) /\
            (s.c_BranchToPCC s.procID = NONE) /\
            (s.c_state.c_BranchDelay <> NONE ==> (s.c_BranchDelayPCC s.procID = NONE)) /\
            (s.c_BranchDelayPCC s.procID <> NONE ==> (s.c_state.c_BranchDelay = NONE))`

val Unchanged_def =
  Define `Unchanged s0 s =
            (s0.c_state.c_BranchTo = s.c_state.c_BranchTo) /\
            (s0.c_BranchToPCC s.procID = s.c_BranchToPCC s.procID) /\
            (s0.c_state.c_BranchDelay = s.c_state.c_BranchDelay) /\
            (s0.c_BranchDelayPCC s.procID = s.c_BranchDelayPCC s.procID)`
            
(* Processor exceptions appear in quite a few places, so in general we'll need to know
   that the information is preserved, or that it has been cleared. *)
val Normal_def =
  Define `Normal s0 s = Unchanged s0 s \/ Empty s`

val invariant_defs = [Empty_def,OneTo_def,OneDelay_def,Unchanged_def,Normal_def]

(* The initial state variable must match the one below in mk_goal, and shouldn't
   appear in the model. *)
val type_invariants = [
  (``:cheri_state``, ``Normal initial_state``)
]


val simp =
    FULL_SIMP_TAC (srw_ss()) (boolTheory.LET_THM::combinTheory.UPDATE_APPLY::invariant_defs)

fun final_step gl _ _ t =
    (simp THEN NO_TAC) gl

val invariant_breakers = [
   "write'BranchDelay_def",
   "write'BranchTo_def",
   "write'BranchDelayPCC_def",
   "write'BranchToPCC_def"
]



fun override_spec c =
       Lib.total (Lib.assoc c) [
          (``CheckBranch``, ``Empty (SND (CheckBranch s))``)
       ]

val special = [
   (``SignalException``, fn (_,step) =>
                            REPEAT (step (fn tm => is_comb tm andalso
                                                   fst (strip_comb tm) = ``write'BranchToPCC``))
                                   THEN
                            SIMP_TAC (srw_ss()) [Normal_def,Empty_def,
                                                 cheriTheory.write'BranchToPCC_def,
                                                 cheriTheory.write'BranchDelayPCC_def,
                                                 cheriTheory.write'BranchTo_def,
                                                 cheriTheory.write'BranchDelay_def,
                                                 combinTheory.UPDATE_APPLY])
]

val setup = {
   type_invariants = type_invariants,
   final_step = final_step,
   invariant_breakers = invariant_breakers,
   override_spec = override_spec,
   special = special
}

val proofs = BulkProof.bulk_prove setup
val () = BulkProof.report proofs

(*

fun er tac =
  let fun aux p =
    case (SOME (e tac)) handle _ => NONE of
      NONE => p
    | SOME p' => aux p'
  in case (SOME (e tac)) handle _ => NONE of
       NONE => e ALL_TAC
     | SOME p => aux p
  end

open CoreTactics CHERIBranches BulkProof

val proofs : (term, thm option) Redblackmap.dict = Redblackmap.mkDict same_const_cmp

val step = fn stop => step_tac type_invariants final_step proofs stop

manual setup ``CheckBranch``

REPEAT (step (fn tm => is_comb tm andalso fst (strip_comb tm) = ``write'BranchToPCC``))
SIMP_TAC (srw_ss()) [Normal_def,Empty_def,
                     cheriTheory.write'BranchToPCC_def,
                     cheriTheory.write'BranchDelayPCC_def,
                     cheriTheory.write'BranchTo_def,
                     cheriTheory.write'BranchDelay_def,
                     combinTheory.UPDATE_APPLY]
(fn gl => final_step gl () () ())

*)
end
end
