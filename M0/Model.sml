structure Model : sig
  include Model
  val ModelInvariant_def : Thm.thm
end =
struct

open HolKernel Parse boolLib bossLib

val ModelInvariant_def =
  Define `ModelInvariant s = ((1 >< 0) (s.REG RName_SP_process) = 0w : word2) /\
                             ((1 >< 0) (s.REG RName_SP_main) = 0w : word2)`;

val invariant_defs = [ModelInvariant_def]

val type_invariants = [(``:m0_state``,``ModelInvariant``)]

fun final_step gl _ _ _ =
  (FULL_SIMP_TAC (srw_ss()) (combinTheory.UPDATE_APPLY::invariant_defs) THEN NO_TAC) gl

val state_ty = ``:m0_state``

val thy = "m0"

val inventory = m0Theory.inventory

val invariant_breakers = ["write'SP_process","write'SP_main"]

(* Semi-automatic cases *)

val override_spec = K NONE

val special : (term * ((term, thm option) Redblackmap.dict * ((term -> bool) -> tactic) -> tactic)) list = 
 [(``write'R``, fn (_,step) =>
  REPEAT (step (K false))
  THENL
  [SIMP_TAC std_ss [m0Theory.LookUpSP_def]
   THEN COND_CASES_TAC
   THEN FULL_SIMP_TAC (srw_ss()) [ModelInvariant_def,combinTheory.UPDATE_APPLY]
   THEN CONV_TAC wordsLib.WORD_CONV,

   wordsLib.Cases_on_word_value `r` THEN
   FULL_SIMP_TAC (srw_ss()) [ModelInvariant_def,combinTheory.UPDATE_APPLY,m0Theory.num2RName_thm]
  ]),
  (``dfn'MoveToSpecialRegister``, fn (_,step) =>
   REPEAT (step (K false))
   THEN FULL_SIMP_TAC (srw_ss()) [ModelInvariant_def,combinTheory.UPDATE_APPLY]
   THEN CONV_TAC wordsLib.WORD_CONV
   ),
  (``PopStack``, fn (_,step) =>
   REPEAT (step (Utils.match_let_binding `spmask:word32`))
   THEN ONCE_REWRITE_TAC [boolTheory.LET_THM]
   THEN REPEAT (step (K false))
   THEN FULL_SIMP_TAC (srw_ss()++wordsLib.WORD_ss) [m0Theory.SP_main_def,m0Theory.SP_process_def,ModelInvariant_def,combinTheory.UPDATE_APPLY]
   THEN blastLib.FULL_BBLAST_TAC
   ),
  (* NB: not required by Next at present *)
  (``PushStack``, fn (_,step) =>
   REPEAT (step (Utils.term_matches_pat `let (v,s) = _ in let s = (_,_,write'SP_process _ _) in _`))
   THEN ONCE_REWRITE_TAC [boolTheory.LET_THM]
   THEN PairRules.PBETA_TAC
   THEN REPEAT (step (Utils.term_matches_pat `write'SP_process _ _`))
   THEN FULL_SIMP_TAC (srw_ss()++wordsLib.WORD_ss) [m0Theory.SP_main_def,m0Theory.write'SP_process_def,m0Theory.SP_process_def,ModelInvariant_def,combinTheory.UPDATE_APPLY,boolTheory.LET_THM]
   THEN blastLib.FULL_BBLAST_TAC
   )
 ];

val default_invariant_setup = {
   type_invariants = type_invariants,
   final_step = final_step,
   invariant_breakers = invariant_breakers,
   override_spec = override_spec,
   definitions = Utils.thy_defns thy (#C inventory),
   special = special
}

end
