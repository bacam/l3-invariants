load "l3inv";
open CoreTactics;

val defns = Utils.thy_defns Model.thy (#C Model.inventory)

val init = Define `Init v s = ((1 >< 0) ((FST (MemA (v,4) s)) : word32) = 0w : word2)`

val init_step = step_tac [(``:m0_state``, ``Init s.VTOR``)]
                         (#final_step Model.default_invariant_setup)
                         defns
                         (Redblackmap.mkDict Utils.same_const_cmp)
                         (Utils.term_matches_pat `LET _ (state.VTOR)`)

val step = BulkProof.mk_step Model.default_invariant_setup proofs;


val MemA_unchanged =
    Tactical.prove
       (``!s s'. 
          (s.AIRCR.ENDIANNESS = s'.AIRCR.ENDIANNESS) /\
          (s.MEM = s'.MEM) /\
          ((1 >< 0) (FST (MemA (x,y) s) : word32) = 0w : word2) ==>
          ((1 >< 0) (FST (MemA (x,y) s') : word32) = 0w :word2)``,
        STRIP_TAC THEN STRIP_TAC THEN STRIP_TAC THEN EVERY_ASSUM (UNDISCH_TAC o concl) THEN
        Cases_on `Aligned (x,y)` THEN
        REPEAT
         (CHANGED_TAC
             (FULL_SIMP_TAC (srw_ss()++boolSimps.BOOL_ss)
                           [m0Theory.MemA_def,m0Theory.mem_def,m0Theory.mem1_def,
                            boolTheory.LET_THM,
                            m0Theory.BigEndianReverse_def,m0Theory.raise'exception_def]
              THEN BasicProvers.EVERY_CASE_TAC)));

val init_unchanged =
    Tactical.prove
       (``!s1 s2.
          (s1.AIRCR.ENDIANNESS = s2.AIRCR.ENDIANNESS) /\
          (s1.MEM = s2.MEM) /\
          Init s.VTOR s1 ==>
          Init s.VTOR s2``,
        STRIP_TAC THEN STRIP_TAC THEN STRIP_TAC THEN EVERY_ASSUM (UNDISCH_TAC o concl) THEN
        SRW_TAC [] [init]
        THEN MATCH_MP_TAC (SPECL [``s1:m0_state``,``s2:m0_state``] MemA_unchanged)
        THEN SRW_TAC [] []);

val def = TRY ((fn (gl as (_,g)) => 
                   ASM_SIMP_TAC (srw_ss()) [SPECL [rand (rand g), rand g] init_unchanged] gl)
                  THEN FULL_SIMP_TAC (srw_ss()) [init_unchanged]
                             THEN NO_TAC)

val m0_okish =
  Tactical.prove
    (``Init s.VTOR s ==> ModelInvariant (TakeReset () s)``,
     REPEAT init_step
     THEN CONV_TAC (RAND_CONV PairedLambda.GEN_LET_CONV)
     THEN (fn (gl as (_,g)) =>
              let val tm = find_term (Utils.term_matches_pat `MemA (s.VTOR,4) _`) g
                  val s = rand tm
              in (KNOW_TAC ``Init s.VTOR ^s``
                  THENL [REPEAT init_step THEN def,
                         SPEC_TAC (s,``s1:m0_state``) THEN REPEAT STRIP_TAC]) gl
              end)
     THEN CONV_TAC (RAND_CONV PairedLambda.GEN_LET_CONV)
     THEN (step (K false))
     THENL
     [step (K false)
      THEN step (K false)
      THEN FULL_SIMP_TAC (srw_ss()) [m0Theory.write'SP_main_def,m0Theory.write'SP_process_def,
                                     init,Model.ModelInvariant_def,combinTheory.APPLY_UPDATE_THM]
      THEN blastLib.BBLAST_TAC,
      REPEAT (step (K false))
     ]);

val m0_okish' = REWRITE_RULE [init] m0_okish;
