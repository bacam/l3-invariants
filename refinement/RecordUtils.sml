structure RecordUtils = struct

local open HolKernel boolSyntax boolLib bossLib
in

(* Based on code in TypeBasePure *)

(*---------------------------------------------------------------------------*)
(* The function                                                              *)
(*                                                                           *)
(*   dest_record_upds : tyBase -> term -> term * (string * term) list        *)
(*                                                                           *)
(* needs to know about the TypeBase in order to tell if the term is an       *)
(* element of a record type.                                                 *)
(*---------------------------------------------------------------------------*)

fun mk_K_1 (tm,ty) =
  let val K_tm = prim_mk_const{Name="K",Thy="combin"}
  in mk_comb(inst [alpha |-> type_of tm, beta |-> ty] K_tm,tm)
  end;
fun dest_K_1 tm =
  let val K_tm = prim_mk_const{Name="K",Thy="combin"}
  in dest_monop K_tm (ERR "dest_K_1" "not a K-term") tm
  end;

fun get_field_name s1 s2 =
  let val prefix = String.extract(s2,0,SOME(String.size s1))
      val rest = String.extract(s2,String.size s1 + 1, NONE)
      val middle = String.extract(rest,0,SOME(String.size rest - 5))
      val suffix = String.extract(rest,String.size middle, NONE)
  in
    if prefix = s1 andalso suffix = "_fupd"
      then middle
      else raise ERR "get_field" ("unable to parse "^Lib.quote s2)
  end;

(*---------------------------------------------------------------------------*)
(* A record looks like `fupd arg_1 (fupd arg_2 ... (fupd arg_n tm) ...)`     *)
(* where each arg_i is a (name,type) pair showing how the ith field should   *)
(* be declared.                                                              *)
(*---------------------------------------------------------------------------*)

fun dest_field tm =
  let val (ty,_) = dom_rng (type_of tm)
      val tyname = fst(dest_type ty)
      val (updf,arg) = dest_comb tm
      val (name0,ty) = dest_const updf
      val name = get_field_name tyname name0
  in
    (name,dest_K_1 arg)
  end
  handle HOL_ERR _ => raise ERR "dest_field" "unexpected term structure";


fun dest_record_upds_pure tybase tm =
  let fun dest tm =
       if not (is_comb tm) then (tm,[])
       else let val (f,a) = dest_comb tm in
            case Lib.total dest_field f of
                NONE => (tm,[])
              | SOME fl =>
                let val (tm',a') = dest a
                in (tm',fl::a')
                end
            end
       handle HOL_ERR _ => raise ERR "dest_record" "unexpected term structure"
  in
   if TypeBasePure.is_record_type tybase (type_of tm)
     then dest tm
     else raise ERR "dest_record" "not a record"
  end;

fun dest_record_upds x = dest_record_upds_pure (TypeBase.theTypeBase()) x

fun type_names ty =
  let val {Thy,Tyop,Args} = Type.dest_thy_type ty
  in (Thy,Tyop)
  end;

fun mk_record_upds_pure tybase (tm,fields) =
 let val ty = type_of tm in
 if TypeBasePure.is_record_type tybase ty
 then let val (Thy,Tyop) = type_names ty
        val fupds = map (fn p => String.concat[Tyop,"_",fst p,"_fupd"]) fields
        val updfns = map (fn n => prim_mk_const{Name=n,Thy=Thy}) fupds
        fun ifn c = let val (_,ty') = strip_fun (type_of c)
                        val theta = match_type ty' ty
                    in inst theta c
                    end
        val updfns' = map ifn updfns
        fun mk_field (updfn,v) tm =
              mk_comb(mk_comb(updfn, mk_K_1(v,type_of v)),tm)
       in
         itlist mk_field (zip updfns' (map snd fields)) tm
       end
  else raise ERR "mk_record_upds_pure" "first arg. not of a record type"
 end;

fun mk_record_upds x = mk_record_upds_pure (TypeBase.theTypeBase()) x

(* Create a list of terms of the form v.field = v'.field *)
fun base_fields v v' =
    let val fns = utilsLib.accessor_fns (type_of v)
    in map (fn tm => mk_eq (mk_comb (tm,v), mk_comb (tm,v'))) fns
    end

(* Update a list of v'.field = ... terms with the updates to v' in tm, using
   v as the variable for the updated state. *)
fun fieldify tm v prev =
    let val ty = type_of tm
        val ty' = dest_thy_type ty
        val (base,upds) = dest_record_upds tm
        val _ = assert (term_eq (rand (lhs (hd prev)))) base
        val fields = TypeBase.fields_of ty
        val base_fields = List.filter (fn (name,_) => List.all (fn (name',_) => name <> name') upds) fields
        fun mk_accessor fld fty = mk_thy_const {Name = (#Tyop ty') ^ "_" ^ fld, Thy = #Thy ty', Ty = ty --> fty}
        fun get_prev (fld, fty) =
            let val acc = mk_accessor fld fty
                val eqn = Option.valOf (List.find (fn tm => term_eq (rator (lhs tm)) acc) prev)
            in (fld, rhs eqn)
            end
        fun mk_proj tm fld fty = mk_comb (mk_accessor fld fty, tm)
        val base_contents = map get_prev base_fields
        fun mk_tm (name,content) =
            mk_eq (mk_proj v name (type_of content), content)
    in map mk_tm (upds@base_contents)
    end

(* TODO: would ideally like this to avoid pushing terms into the hypotheses,
         and restrict rewrites to the relevant terms *)
fun INST_record v tm th =
    let val ty = type_of v
        val ty' = dest_thy_type ty
        val accfupds = DB.fetch (#Thy ty') (#Tyop ty' ^ "_accfupds")
        fun mk_accessor (fld, fty) =
            mk_comb (mk_thy_const {Name = (#Tyop ty') ^ "_" ^ fld, Thy = #Thy ty', Ty = ty --> fty},
                     v)
        val v_tm_eq = ASSUME (mk_eq (v,tm))
        fun field fl = PURE_REWRITE_CONV [v_tm_eq,accfupds,combinTheory.K_THM] (mk_accessor fl)
        val ths = map field (TypeBase.fields_of ty)
        val th' = th |> DISCH_ALL |> PURE_REWRITE_RULE ths |> INST [v |-> tm] |> UNDISCH_ALL
    in PROVE_HYP (REFL tm) th'
    end

end

end
