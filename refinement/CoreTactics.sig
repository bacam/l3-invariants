signature CoreTactics =
sig

include Abbrev

val find_invs_tm : (hol_type * term) list -> term -> term list
val find_invs_tuple : (hol_type * 'a) list -> term -> term -> (term * term * 'a) list

val step_tac :
    (* List of types and predicates that should hold for them *)
    (hol_type * term) list ->
    (* What to do when we've reached a goal that we actually want to prove *)
    (goal -> (term -> term) -> (conv -> conv) -> term -> goal list * validation) ->
    (* Map of definitions which can be unfolded *)
    (term, thm) Redblackmap.dict ->
    (* Map of constants to previously proved theorems (the option is for
       compatibility with the main invariant proving code, NONE values are
       treated the same as missing values here) *)
    (term, thm option) Redblackmap.dict ->
    (* Stop if the term matches this predicate *)
    (term -> bool) ->
    tactic

end
