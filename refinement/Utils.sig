signature Utils =
sig

include Abbrev

(* Test whether a term (2nd arg) matches the given pattern (1st arg) *)
val term_matches_pat : term quotation -> term -> bool

(* Check if a let term has bindings matching the given variable/tuple,
   insisting that names match unless they start with _, and that types
   match up to instantiation of type variables. *)
val match_let_binding : term quotation -> term -> bool

(* Ordering used for the previously proved theorems map *)
val same_const_cmp : term * term -> order

val thy_defns : string -> string list -> (term,thm) Redblackmap.dict

val MAP_CONV_TAC : conv -> tactic

end
