structure BulkProof =
struct

local
open HolKernel Parse boolLib bossLib;
open CoreTactics;
in

fun mk_auto_goal tyinvs c =
    let val (argtys,rty) = wfrecUtils.strip_fun_type (type_of c)
        val invs_vars = free_varsl (map snd tyinvs)
        fun addvar (ty,vars) = (variant (invs_vars@vars) (mk_var (fst (dest_type ty),ty)))::vars
        val args = foldr addvar [] argtys
        val hyps = List.concat (map (find_invs_tm tyinvs) args)
        val app = list_mk_comb (c,args)
        val goalr = list_mk_conj (find_invs_tm tyinvs app)
       (* These have to be t1 /\ ... /\ tn ==> t1' /\ ... tm' rather than
          curried so that we can use MATCH_MP_TAC. *)
    in case hyps of [] => goalr
                  | _ => mk_imp (list_mk_conj hyps,goalr)
    end

fun mk_goal (setup:BulkProofTypes.setup) c =
    case #override_spec setup c of
        NONE => mk_auto_goal (#type_invariants setup) c
      | SOME spec => spec

(* Does a constant look like there's an interesting theorem to prove? *)
fun check_const setup (const,th) =
  let val goal = mk_goal setup const
  in SOME (const,concl th)
  end handle _ => NONE


(* Lib.dict_topsort, but altered to only return keys to avoid instantiating type variables *)
fun dict_topsort deps =
   let
      open Redblackmap
      val deps = transform (fn ls => ref (SOME ls)) deps
      fun visit (n, ls) =
         let
            val (n,r) = findKey (deps, n)
         in
            case !r of
               NONE => ls
             | SOME dl => let
                             val _ = r := NONE
                             val ls = List.foldl visit ls dl
                          in
                             n :: ls
                          end
         end
      fun v (n, _, ls) = visit (n, ls)
   in
      foldl v [] deps
   end

(* Turn a map of constants to definitions into a map from constants to constants they depend upon *)

fun deps const_defs (c,tm) =
    find_terms (fn tm => is_const tm andalso
                         not (same_const tm c) andalso
                         Option.isSome (Redblackmap.peek (const_defs,tm))) tm

fun make_deps const_defs =
  Redblackmap.map (deps const_defs) const_defs

(* Try an automatic proof *)

fun try setup const_deps progress c =
    let val deps = Redblackmap.find (const_deps, c)
        val () = print (term_to_string c ^ ": ")
    in if List.all (fn tm => Option.isSome (Redblackmap.find (progress, tm))) deps
       then
          let val goal = mk_goal setup c
              val tac = (Lib.assoc c (#special setup))
                        handle _ => (fn (_,step) =>
                                        REPEAT (step (K false))
                                        (* Handle simple side conditions automatically
                                           (e.g., from induction hypotheses) *)
                                        THEN TRY (FULL_SIMP_TAC std_ss [] THEN NO_TAC))
              val step = step_tac (#type_invariants setup) (#final_step setup)
                                  (#definitions setup) progress
              val th = Tactical.prove (goal, tac (progress, step))
              val () = print "OK\n"
          in SOME th
          end handle HOL_ERR _ =>
                     (print "failed\n"; NONE)
                   | Interrupt =>
                     (print "interrupted\n"; NONE)
       else (print "skipped\n"; NONE)
    end

fun process setup const_deps (c,m) =
    let val r = try setup const_deps m c
    in Redblackmap.insert (m,c,r)
    end

fun bulk_prove setup =
    let val cds = List.mapPartial (check_const setup)
                                  (filter (not o C mem (#invariant_breakers setup) o
                                           fst o dest_const o fst)
                                          (Redblackmap.listItems (#definitions setup)))
        val cds = Redblackmap.fromList Utils.same_const_cmp cds
        val cdeps = make_deps cds
        val const_order = dict_topsort cdeps
        val proofs : (term, thm option) Redblackmap.dict = Redblackmap.mkDict Utils.same_const_cmp
        val proofs = foldr (process setup cdeps) proofs const_order
    in proofs
    end

local
fun count (_,NONE,(m,n)) = (m,n+1)
  | count (_,SOME _,(m,n)) = (m+1,n+1)
in
fun report proofs =
    let val (m,n) = Redblackmap.foldl count (0,0) proofs
        val () = print ("Found proofs for " ^ Int.toString m ^
                        " out of " ^ Int.toString n ^ " constants.\n")
    in case Redblackmap.find (proofs,``Next``) of
           NONE => print "No proof for Next found!\n"
         | SOME th => (print "Result for Next:\n\n"; print_thm th; print "\n")
    end
end



(* Set up a goal for interactive proof *)

fun manual setup c = proofManagerLib.set_goal ([], mk_goal setup c)

(* Specialised versions of step_tac *)
fun mk_step (setup:BulkProofTypes.setup) proofs stop = 
    step_tac (#type_invariants setup)
             (#final_step setup)
             (#definitions setup)
             proofs
             stop

end
end
