structure CoreTactics : CoreTactics =
struct

local
open HolKernel Parse boolLib bossLib;
in

open Abbrev

val FOR_inv = Tactical.prove (
  ``!P v.
    i <= j /\
    P ((),v) /\
    (!k x. i <= k ==> k <= j ==> P ((),x) ==> P (f k x)) ==>
    P (FOR (i,j,f) (v:'a))``,
  STRIP_TAC THEN
  SUBGOAL_THEN ``!x:unit # 'a. P x ==> P ((),SND x)`` ASSUME_TAC
  THENL
  [Cases
   THEN Cases_on `q`
   THEN SIMP_TAC std_ss [],
   Induct_on `j-i`
   THENL
   [ONCE_REWRITE_TAC [state_transformerTheory.FOR_def]
    THEN SIMP_TAC arith_ss [],
    REPEAT STRIP_TAC
    THEN ONCE_REWRITE_TAC [state_transformerTheory.FOR_def]
    THEN ASM_SIMP_TAC arith_ss [state_transformerTheory.BIND_DEF,pairTheory.UNCURRY]]])

val pair_CASE_let = Tactical.prove (
  ``pair_CASE (p:'a # 'b) (f:'a -> 'b -> 'c) = (let (x,y) = p in f x y)``,
  ONCE_REWRITE_TAC [GSYM pairTheory.PAIR]
  THEN SIMP_TAC std_ss [pairTheory.pair_CASE_def,boolTheory.LET_THM])

(* Our treatment of (e.g.) lets will leave behind hypotheses about variables
   that have disappeared from the goal, so we tidy them up.  This is specialised
   to our predicates, because then we can be fairly certain that they're not
   (e.g.) a false term that we haven't discovered yet. *)
fun weaken_irrelevant tyinvs =
    let val predicates = map snd tyinvs
    in fn gl =>
          let fun irrelevant tm =
                  is_comb tm andalso
                  List.exists (term_eq (rator tm)) predicates andalso
                  is_var (rand tm) andalso
                  not (mem (rand tm) (free_vars (snd gl)))
          in REPEAT (WEAKEN_TAC irrelevant) gl
          end
    end

(* Strip down the desired conclusion to find the actual computation that we're
   interested in.  Returns a function to rebuild the conclusion with a changed
   subterm, a function to apply a conversion at the subterm and the subterm
   itself. *)
fun strip_goal_tm tm =
    let val (inv,t) = dest_comb tm (* strip off predicate *)
        (* Strip off outer projection of state, remember how to get back *)
        fun unproj cons conv tm =
            if pairSyntax.is_fst tm
            then let val tm' = pairSyntax.dest_fst tm
                 in if pairSyntax.is_pair tm'
                    then (cons,conv,tm)
                    else unproj (cons o pairSyntax.mk_fst) (conv o RAND_CONV) tm'
                 end
            else if pairSyntax.is_snd tm
            then let val tm' = pairSyntax.dest_snd tm
                 in if pairSyntax.is_pair tm'
                    then (cons,conv,tm)
                    else unproj (cons o pairSyntax.mk_snd) (conv o RAND_CONV) tm'
                 end
            else (cons,conv,tm)
    in unproj (curry mk_comb inv) RAND_CONV t
    end

local
fun find_in_var tyinvs ty =
  case Lib.assoc1 ty tyinvs of
      NONE =>
      (case (SOME (pairSyntax.dest_prod ty)) handle _ => NONE of
           NONE => []
         | SOME (ty1,ty2) =>
           (map (fn (f,inv) => (f o pairSyntax.mk_fst,inv)) (find_in_var tyinvs ty1))@
           (map (fn (f,inv) => (f o pairSyntax.mk_snd,inv)) (find_in_var tyinvs ty2)))
    | SOME (_,inv) => [(I,inv)]
in
fun find_invs_tm tyinvs tm =
    let val invs = find_in_var tyinvs (type_of tm)
    in map (fn (f,inv) => mk_comb (inv, f tm)) invs
    end

fun find_invs_tuple tyinvs tm tm' =
    if is_var tm then map (fn (f,inv) => (f tm, f tm', inv)) (find_in_var tyinvs (type_of tm))
    else let val (tm1,tm2) = pairSyntax.dest_pair tm
             val (tm1',tm2') = (pairSyntax.dest_pair tm')
                               handle _ => (pairSyntax.mk_fst tm',pairSyntax.mk_snd tm')
         in (map (fn (v,tm,inv) => (v,tm,inv)) (find_invs_tuple tyinvs tm1 tm1'))@
            (map (fn (v,tm,inv) => (v,tm,inv)) (find_invs_tuple tyinvs tm2 tm2'))
         end

 (* Heuristic: squash term to a variable if we have a refinement of
    its type, otherwise keep it.  Look at subterms if an term does not
    have a refinement. *)
fun find_invs_for_subs tyinvs hyps tm =
  let val var = if is_var tm then tm else genvar (type_of tm)
      fun filter_hyps (hyps',tm') =
        let val hyps'' = filter (fn (x,v,t) => not (List.exists (term_eq (subst [v |-> t] x)) hyps)) hyps'
        in if List.null hyps'' then ([],tm) else (hyps'',tm')
        end
  in if is_arb tm then ([],tm) else
     case Lib.assoc1 (type_of tm) tyinvs of
         NONE =>
         if pairSyntax.is_pair tm
         then let val (l,r) = pairSyntax.dest_pair tm
                  val (li,lt) = find_invs_for_subs tyinvs hyps l
                  val (ri,rt) = find_invs_for_subs tyinvs hyps r
              in (li@ri,pairSyntax.mk_pair (lt,rt))
              end
         (* Once we have exhausted exploration of the term, look at the type to
            form goals based on projections, etc *)
         else let val invs = find_in_var tyinvs (type_of tm)
                  val invs = map (fn (f,inv) => (mk_comb (inv, f var),var,tm)) invs
              in if List.null invs
                 (* If the current term is completely boring, look at subterms
                 (TODO: consider whether this fits in neatly with the presentation, 
                        or if there's a nicer thing to do) *)
                 then if is_comb tm
                      then let val (li,lt) = find_invs_for_subs tyinvs hyps (rator tm)
                               val (ri,rt) = find_invs_for_subs tyinvs hyps (rand tm)
                           in (li@ri,mk_comb (lt,rt))
                           end
                      else ([], tm)
                 else filter_hyps (invs, var)
              end
       | SOME (_,inv) =>
         filter_hyps ([(mk_comb (inv,var),var,tm)],var)
  end
end

(* Avoids renaming two variables to the same thing *)
fun variantl avoid [] = []
  | variantl avoid (h::t) = 
    let val h' = variant avoid h
    in h'::(variantl (h'::avoid) t)
    end

(* Based on internal flat_vstruct function from pairTools >>>>>> *)
local open pairSyntax in

(*---------------------------------------------------------------------------
 * Builds a substitution of projection terms for "rhs", based on structure
 * of "tuple".

       fun flat_vstruct0 tuple rhs =
          if (is_var tuple) then [rhs]
          else let val {fst,snd} = dest_pair tuple
               in flat_vstruct0 fst (mk_fst rhs) @
                  flat_vstruct0 snd (mk_snd rhs)
               end;

 * That was the clean implementation. Now for the one that gets used, we
 * add an extra field to the return value, so that it can be made into
 * an equality.
 *---------------------------------------------------------------------------*)

fun flat_vstruct_subs tuple rhs =
  let
    (* behaviour of mk_fst and mk_snd should match PairedLambda.GEN_BETA_CONV in LET_INTRO *)
    val mk_fst = fn tm => if is_pair tm then #1 (dest_pair tm) else mk_fst tm
    val mk_snd = fn tm => if is_pair tm then #2 (dest_pair tm) else mk_snd tm
    fun flat tuple (v,rhs) =
      if is_var tuple then [(tuple, rhs)]
      else let val (fst,snd) = dest_pair tuple
           in  flat fst (v, mk_fst rhs) @
               flat snd (v, mk_snd rhs)
           end
  in map (op |->) (flat tuple (genvar alpha,rhs))
  end;

end (* <<<<<<<<<<< *)

(* Based on Conv.MAP_THM >>>> *)

(*---------------------------------------------------------------------------*
 * Map a conversion over a theorem, preserving order of hypotheses.          *
 *---------------------------------------------------------------------------*)

local
   fun cnvMP eqth impth =
      let
         open boolSyntax
         val tm = snd (dest_imp (concl impth))
         val imp_refl = REFL implication
      in
         UNDISCH (EQ_MP (MK_COMB (MK_COMB (imp_refl, eqth), REFL tm)) impth)
      end
in
   fun MAP_THM_HYP cnv th =
      let
         val (hyps, c) = dest_thm th
         val hypths = map cnv hyps
      in
         itlist cnvMP hypths (DISCH_ALL th)
      end
      handle HOL_ERR _ => raise ERR "MAP_THM_HYP" ""
end

(* <<<< *)

fun let_step tyinvs (hs,tm) cons conv t =
    let val (tuple,m,n) = pairSyntax.dest_plet t
        val invs = find_invs_tuple tyinvs tuple m
        val new_subgoals = map (fn (_,tm,inv) => mk_comb (inv, tm)) invs

        (* Arrange sufficient renaming to avoid clashes *)
        val bound_vars = pairSyntax.strip_pair tuple
        val old_vars = free_varsl (tm::hs)
        val new_vars = variantl old_vars bound_vars
        val subsv = map (op |->) (ListPair.zip (bound_vars, new_vars))
        val final_subgoal = subst subsv (cons n)

        (* Calculate the new hypotheses that the final subgoal will get *)
        val new_hyps = map (fn (v,_,inv) => mk_comb (inv,subst subsv v)) invs
        val final_subgoal = list_mk_imp (new_hyps, final_subgoal)

        val substm' = flat_vstruct_subs tuple m
        val substm' = map (fn {redex,residue} => {redex=subst subsv redex,residue=residue}) substm'
        val substm = map (fn {redex,residue} => {redex=residue,residue=redex}) substm'

        (* Update any hypotheses that depend on m *)
        val final_hs = map (subst [m |-> subst subsv tuple]) hs

        (* and replace any occurances of m in the final subgoal *)
        val template_var = genvar (type_of m)
        val final_subgoal_template = subst [m |-> template_var] final_subgoal
        val final_subgoal = subst [template_var |-> subst subsv tuple] final_subgoal_template

        val letrw =
            SYM (conv (ONCE_REWRITE_CONV [boolTheory.LET_THM]
                       THENC PairedLambda.GEN_BETA_CONV) tm)

        fun justify (ths as _::_) =
          let val (hyps,th1) = Lib.front_last ths

              (* Undo substitution of m in the hypotheses, then the conclusion *)
              val hseq = mk_eq (subst subsv tuple,m)
              val th2 = MAP_THM_HYP (QCONV (PURE_REWRITE_CONV [ASSUME hseq])) th1
              val th3 = SUBST [template_var |-> ASSUME hseq] final_subgoal_template th2

              val th4 = INST substm' th3
              val hsth = simpLib.SIMP_PROVE (srw_ss()) [] (subst substm' hseq)
              val th5 = PROVE_HYP hsth th4

              val th6 = LIST_MP hyps th5
          in ONCE_REWRITE_RULE [letrw] th6
          end
          | justify _ = fail ()
                             
    in ((map (fn tm => (hs,tm)) new_subgoals)@[(final_hs,final_subgoal)], justify)
    end

local fun is_constructor tm = not (is_var tm orelse pairSyntax.is_pair tm)
in
  (* There won't be a recursion induction theorem if the definition is primitive recursive;
     try looking for the appropriate argument in the same way as non_wfrec_defn
     in src/tfl/src/Defn.sml *)
fun primrec_arg def =
  let val args = (snd o strip_comb o lhs o snd o strip_forall o hd o strip_conj o concl) def
  in index is_constructor args
  end 
end

fun const_step tyinvs final_step defns preproved gl cons conv t =
    let val (c,args) = strip_comb t
        fun use_existing th =
          let val () = print ("Using preproved theorem for " ^ term_to_string c ^"\n")
              val ths = map DISCH_ALL (CONJUNCTS (UNDISCH_ALL th))
              (* If the theorem isn't an implication, then check if it solves the goal directly *)
              fun match_tac th = (MATCH_MP_TAC th) handle _ => MATCH_ACCEPT_TAC th
          in FIRST (map match_tac ths) gl
          end
        fun unfold_defn th =
          let val pats = (snd o strip_comb o lhs o snd o strip_forall o concl) th
              fun check [] _ = NONE
                | check _ [] = NONE
                | check (arg::args) (pat::pats) =
                  if is_var pat
                  then check args pats
                  else if pairSyntax.is_pair pat
                  then if pairSyntax.is_pair arg
                       then check (list_of_pair (pairSyntax.dest_pair arg) @ args)
                                  (list_of_pair (pairSyntax.dest_pair pat) @ pats)
                       else SOME arg
                  else failwith ("Don't know how to split " ^ term_to_string pat)
          in case check args pats of
                 NONE => ONCE_REWRITE_TAC [th] gl
               | SOME arg => Cases_on `^arg` gl
          end
        fun induction defth indth =
          (schneiderUtils.UNDISCH_ALL_TAC
           THEN MAP_EVERY (fn x => SPEC_TAC (x,x)) (rev args)
           THEN recInduct indth
           (* Be careful to rewrite only the conclusions, not the IH, and avoid
              rewriting inside *)
           THEN CONV_TAC (EVERY_CONJ_CONV (STRIP_QUANT_CONV (RAND_CONV (ONCE_REWRITE_CONV [defth]))))) gl
        fun primrec_ind defth =
          let val ind_arg = List.nth (args, primrec_arg defth)
              val args' = filter (not o (term_eq ind_arg)) args
          in (schneiderUtils.UNDISCH_ALL_TAC
              THEN MAP_EVERY (fn x => SPEC_TAC (x,x)) (rev args')
              THEN Induct_on [ANTIQUOTE ind_arg]
              THEN CONV_TAC (EVERY_CONJ_CONV (STRIP_QUANT_CONV (RAND_CONV (ONCE_REWRITE_CONV [defth]))))) gl
          end
    in case Redblackmap.peek (preproved,c) of
           SOME (SOME th) => use_existing th
         | _ =>
           let val (subglsinfo,args') = ListPair.unzip (map (find_invs_for_subs tyinvs (fst gl)) args)
               val subglsinfo = List.concat subglsinfo
           in if List.null subglsinfo
              then
                 case Redblackmap.peek (defns,c) of
                     SOME th => 
                     let val c' = dest_thy_const c
                     in ((induction th (DB.fetch (#Thy c') (#Name c' ^ "_ind")))
                         handle _ => primrec_ind th)
                        handle _ => unfold_defn th
                     end
                   | NONE => final_step gl cons conv t
              else
                 let val subgls = map (fn (inv,v,tm) => (fst gl, subst [v |-> tm] inv)) subglsinfo
                     val asms = map (fn (inv,_,_) => inv) subglsinfo
                     val subs = map (fn (_,v,tm) => v |-> tm) subglsinfo
                     val t' = list_mk_imp (asms, cons (list_mk_comb (c,args')))
                     fun justify (th::ths) = LIST_MP ths (INST subs th)
                       | justify [] = failwith "Bind Error"
                 in ((fst gl,t')::subgls, justify)
                 end
           end
    end

fun for_const_step gl cons conv t =
    let val ty = type_of t
        val th = INST_TYPE [alpha |-> snd (pairSyntax.dest_prod ty)] FOR_inv
        val v = genvar ty
        val P = mk_abs (v, cons v) (* FIXME: can probably simplify this *)
        val th = SIMP_RULE std_ss [] (SPEC P th)
    in (MATCH_MP_TAC th THEN SIMP_TAC arith_ss []) gl
    end

(* Complete case terms on a word value usually have an extraneous | v => ARB
   case, which we can avoid by performing case analysis on the actual value.
   The test is a little imprecise; we might need to be more careful in future. *)
fun case_step gl cons conv t =
    let val (tm_on,cases) = TypeBase.strip_case t
    in if wordsSyntax.is_word_type (type_of tm_on) andalso List.exists (is_arb o snd) cases
       then (wordsLib.Cases_on_word_value `^tm_on`
             (* This was originally just SIMP_TAC, but that can strip off the literal_case
                constant from nested case terms *)
             THEN ONCE_REWRITE_TAC [boolTheory.literal_case_THM]
             THEN CONV_TAC (conv BETA_CONV)
             THEN REPEAT (CONV_TAC (conv ((RATOR_CONV o RATOR_CONV o RAND_CONV)
                                             (SIMP_CONV (std_ss++wordsLib.WORD_ss) [])))
                          THEN ONCE_REWRITE_TAC [boolTheory.COND_CLAUSES])) gl
       else if Lib.can pairSyntax.dest_prod (type_of tm_on)
       then ONCE_REWRITE_TAC [pair_CASE_let] gl
       (* Otherwise we'll use the usual case tactic, but only after extracting
          the term we're matching on so that we prove any invariants of its
          contents. *)
       else if is_var tm_on
            (* A more restrained version of TOP_CASE_TAC, except that we use
               CASE_SIMP_CONV on the hypotheses too *)
       then (BasicProvers.PURE_TOP_CASE_TAC THEN Utils.MAP_CONV_TAC BasicProvers.CASE_SIMP_CONV
             (* Simplify to clean up induction hypotheses *)
             THEN FULL_SIMP_TAC bool_ss []) gl
       else let val v = genvar (type_of tm_on)
                val tm = pairSyntax.mk_plet (v,tm_on,subst [tm_on |-> v] t)
                val th = SYM (PairedLambda.let_CONV tm)
            in ONCE_REWRITE_TAC [th] gl
            end
    end

(* We want to process a series of record updates at once, as
individually they may break invariants. *)

fun rec_upd_step tyinvs final_step gl cons conv t =
  let val (subtm,upds) = RecordUtils.dest_record_upds t
      val (subgls,subtm) = find_invs_for_subs tyinvs (fst gl) subtm
      val fields = map (fn (f,tm) => let val (sgls,tm) = find_invs_for_subs tyinvs (fst gl) tm in (sgls,(f,tm)) end) upds
      val fsubgls = List.concat (map fst fields)
      val (subglsinfo,fields) = (subgls @ fsubgls, map snd fields)
      val subgls = map (fn (inv,v,tm) => (fst gl, subst [v |-> tm] inv)) subglsinfo
      val asms = map (fn (inv,_,_) => inv) subglsinfo
      val subs = map (fn (_,v,tm) => v |-> tm) subglsinfo
      val subs' = map (fn (_,v,tm) => tm |-> v) subglsinfo
      val t' = list_mk_imp (asms, cons (RecordUtils.mk_record_upds (subtm,fields)))
      fun justify (th::ths) = LIST_MP ths (INST subs th)
        | justify [] = failwith "Bind Error"
(* TODO: gather together repeated goals *)
  in if List.null subgls
     then final_step gl cons conv t
     else ((map (subst subs') (fst gl),t')::subgls, justify)
  end

(* COND_CASES_TAC leaves constants alone, but if we see one then we want to
   get rid of it *)
fun cond_tac tm  =
  let val (guard,_,_) = dest_cond tm
  in if is_const guard
     then ONCE_REWRITE_TAC [boolTheory.COND_CLAUSES]
     else COND_CASES_TAC
  end

fun select_step tyinvs final_step defns preproved stopat gl cons conv t =
    if stopat t
    then NO_TAC gl
    else if pairSyntax.is_plet t
    (* A bit of tactical cheating... *)
    then ((fn gl => let_step tyinvs gl cons conv t) THEN weaken_irrelevant tyinvs) gl
    else if is_cond t
    then cond_tac t gl
    else if TypeBase.is_case t
    then case_step gl cons conv t
    else if pairSyntax.is_fst t orelse pairSyntax.is_snd t
    then ONCE_REWRITE_TAC [pairTheory.FST,pairTheory.SND] gl
    else if is_var t
         (* If we're lucky, this is one of the assumptions and we can prove it
            quickly, otherwise we'll throw the full final_step tactic at it. *)
    then if exists (term_eq (snd gl)) (fst gl)
         then (FIRST_ASSUM ACCEPT_TAC) gl
         else final_step gl cons conv t
    else
       let val h = fst (strip_comb t)
       in if same_const h state_transformerSyntax.for_tm
          then for_const_step gl cons conv t
          else if is_const h andalso String.isSuffix "_fupd" (fst (dest_const h))
          then rec_upd_step tyinvs final_step gl cons conv t
          else if is_const h
          then const_step tyinvs final_step defns preproved gl cons conv t
          else if is_abs h
          then CONV_TAC (ONCE_DEPTH_CONV BETA_CONV) gl
          else failwith "Don't know how to handle this term"
       end

fun step_tac tyinvs final_step defns preproved stopat =
  FIRST_ASSUM irule ORELSE
  (fn (hs,tm) =>
    if is_forall tm orelse is_imp tm orelse is_conj tm then STRIP_TAC (hs,tm) else
    if term_eq tm T then ACCEPT_TAC TRUTH (hs,tm) else
    let val (cons,conv,subtm) = strip_goal_tm tm
    in select_step tyinvs final_step defns preproved stopat (hs,tm) cons conv subtm
    end)

end
end
