structure Utils : Utils =
struct

open HolKernel Parse boolLib bossLib;

fun term_matches_pat q =
    let val tm = Lib.with_flag (Globals.notify_on_tyvar_guess,false) Parse.Term q
    in Lib.can (ho_match_term [] empty_tmset tm)
    end

(*

> Utils.term_matches_pat `let x = _ in _` ``let x = 10:num in x+y``;
val it = true: bool

Note that _ doesn't work as a wildcard when matching, and causes trouble even
if the term itself uses one:

> Utils.term_matches_pat `let _ = _ in _` ``let x = 10:num in x+y``;
val it = false: bool
> Utils.term_matches_pat `let x = _ in _` ``let _ = 10:num in x+y:num``;
val it = true: bool
> Utils.term_matches_pat `let _ = _ in _` ``let _ = 10:num in x+y:num``;
val it = false: bool

*)

fun match_let_binding q =
    let val tm = Lib.with_flag (Globals.notify_on_tyvar_guess,false) Parse.Term q
        fun match tm' =
            let val (p,_,_) = pairSyntax.dest_plet tm'
                val (term_subst, _) = match_term tm p
            in List.all (String.isPrefix "_" o fst o dest_var o #redex) term_subst
            end handle _ => false
    in match
    end

(*

> match_let_binding `(x:num,_)` ``let (x,y) = (1:num,3:real) in ()``;
val it = true: bool
> match_let_binding `(x:num,_)` ``let (z,y) = (1:num,3:real) in ()``;
val it = false: bool
> match_let_binding `(x:num,z)` ``let (x,y) = (1:num,3:real) in ()``;
val it = false: bool
> match_let_binding `(x,_)` ``let (z,y) = (1:num,3:real) in ()``;
val it = false: bool
> match_let_binding `(x,_)` ``let (x,y) = (1:num,3:real) in ()``;
val it = true: bool

*)

fun same_const_cmp (tm1,tm2) =
  if same_const tm1 tm2
  then EQUAL
  else Term.compare (tm1,tm2)

fun thy_defns thy names =
  let fun fetch s =
        let val th = DB.fetch thy s
            val c = (fst o strip_comb o lhs o snd o strip_forall o concl) th
        in (c,th)
        end
  in Redblackmap.fromList same_const_cmp (map fetch names)
  end


(* Mash up of Conv.MAP_THM and Tactic.CONV_TAC *)

local
   fun cnvMP eqth impth =
      let
         open boolSyntax
         val tm = snd (dest_imp (concl impth))
         val imp_refl = REFL implication
      in
         UNDISCH (EQ_MP (MK_COMB (MK_COMB (imp_refl, eqth), REFL tm)) impth)
      end

   fun empty th [] = th
     | empty th _ = raise ERR "empty" "Bind Error"
   fun sing f [x] = f x
     | sing f _ = raise ERR "sing" "Bind Error"
in

fun MAP_CONV_TAC (conv: conv) : tactic =
   fn (asl, w) =>
      let
         val th = (QCONV conv) w
         val (_, w') = dest_eq (concl th)
         val ths = map (QCONV conv) asl
         val asl' = map (snd o dest_eq o concl) ths
         val ths = filter (fn th => not (List.exists (term_eq (rhs (concl th))) asl)) ths
         fun justify th' =
           itlist (fn eqth => PROVE_HYP (UNDISCH (fst (EQ_IMP_RULE eqth)))) ths (EQ_MP (SYM th) th')
      in
         if w' = T
            then ([], empty (EQ_MP (SYM th) TRUTH))
         else ([(asl', w')], sing justify)
      end
end

end
