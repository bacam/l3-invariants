structure BulkProofTypes =
struct

open Abbrev

type setup = {
   (* Invariants to be associated with each type at let terms *)
   type_invariants : (hol_type * term) list,

   (* What to do when we've reached a goal that we actually want to prove *)
   final_step : goal -> (term -> term) -> (conv -> conv) -> term -> goal list * validation,

   (* Constants that the invariant don't necessarily maintain the invariant *)
   invariant_breakers : string list,

   (* Functions with unusual specifications *)
   override_spec : term -> term option,

   (* Definitions that can be unfolded *)
   definitions : (term, thm) Redblackmap.dict,

   (* How to prove special cases.  For each constant, the list entry consists of
   the constant as a term, and a tactic for the proof which takes the previous
   results and the core step tactic (parametrised by a function telling it
   when to stop) as arguments. *)
   special : (term * ((term, thm option) Redblackmap.dict * ((term -> bool) -> tactic) -> tactic)) list
}

end
