# Proving invariants of L3 ISA models

This work provides proofs of some invariants of two processor
instruction set models written in the L3 language in their HOL4 form.
A paper describing the approach has been submitted to a conference,
and a preprint is available on request.

Last versions tested:

Repository    | git commit / version                       | Available from
------------- | ------------------------------------------ | -----------------------------------------
l3-invariants | `f0ec07d733d1e9092563d16ab7b0379ec457bf0d` | https://bitbucket.org/bacam/l3-invariants
HOL           | `fafed5700eef0112a707a289cf08f1c4b9519394` | https://github.com/HOL-Theorem-Prover/HOL
L3            | `L3-2016-06-24`                            | http://www.cl.cam.ac.uk/~acjf3/l3/
l3mips        | `13ceb7cf8877ecef87f89362be23d8232a4e263c` | https://github.com/acjf3/l3mips

The M0 model and L3 support code is contained in the HOL repository,
in the directory `examples/l3-machine-code`.  For the CHERI model a
small change in the L3 support code in the HOL repository is needed,
see `smallrec.patch`.  The CHERI model should be built with
`make hol CAP=p256`.

## M0 model

To check the invariant:

1. Install HOL
2. Change to the `M0` directory
3. Run `Holmake`
4. Run `l3inv`

To check the establishment proof, run

    load "Establish";

in `hol` and the result will be in the variable `m0_okish'`.  Note
that this will also check the invariant because a few proofs are
reused.

## CHERI model

To check the invariants for the CHERI model:

1. Install all of the software, including the patch
2. Change to the `CHERI` directory
3. Run `Holmake`
4. Run `./l3inv`

The establishment proof can be checked in `hol` by running

    load "Establish";

after which the main result is in the variable
`invariant_established`.
